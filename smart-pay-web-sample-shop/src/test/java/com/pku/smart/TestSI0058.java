package com.pku.smart;

import com.pku.smart.utils.XmlUtils;
import com.pku.smart.vopackage.si0058.VoReqIIH0058Param;
import com.pku.smart.vopackage.si0058.VoReqIIH0058ParamDataEntinfo;
import com.pku.smart.vopackage.si0058.VoReqIIH0058ParamDataEntinfoPresinfo;

import java.util.ArrayList;
import java.util.List;

public class TestSI0058 {
    public static void main(String[] args) {
        VoReqIIH0058Param param = new VoReqIIH0058Param();
        param.setCode_user("00000");
        param.setCode_external("IIH");
        param.getData().setCode_pat("100000011500");
        param.getData().setCode_enttp("01");
        List<VoReqIIH0058ParamDataEntinfo> Entinfos = new ArrayList<>();
        VoReqIIH0058ParamDataEntinfo entinfo = new VoReqIIH0058ParamDataEntinfo();
        entinfo.setCode_enttp("00");
        List<VoReqIIH0058ParamDataEntinfoPresinfo> Presinfos = new ArrayList<>();
        VoReqIIH0058ParamDataEntinfoPresinfo Presinfo = new VoReqIIH0058ParamDataEntinfoPresinfo();
        Presinfo.setAmt_pres("0");
        Presinfos.add(Presinfo);
        entinfo.getPresinfos().setPresinfos(Presinfos);
        Entinfos.add(entinfo);
        param.getData().getEntinfos().setEntinfos(Entinfos);
        String xml = XmlUtils.toXml(param);
        System.out.println(xml);
    }
}
