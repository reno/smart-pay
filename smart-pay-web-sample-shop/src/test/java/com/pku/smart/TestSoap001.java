package com.pku.smart;

import com.alibaba.fastjson.JSON;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

public class TestSoap001 {
    public static void main(String[] args) throws Exception{
        String url = "http://10.100.254.55/iih.hewcxyy.ei.api.i.IApiService?wsdl&access_token=421a963e-2281-466d-867e-20019f9c9e52";
        //String url = "http://10.100.254.28:8089/iih.ei.std.i.IIHService?wsdl&access_token=421a963e-2281-466d-867e-20019f9c9e52";
        String methodName = "apiEntry";
        String[] param = new String[]{"SI0003","<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<iihparam> \n" +
                "  <!--用户编码-->  \n" +
                "  <Code_user>00000</Code_user>  \n" +
                "  <!--科室编码-->  \n" +
                "  <Code_dep>4022</Code_dep>  \n" +
                "  <!--外部编码-->  \n" +
                "  <Code_external>IIH</Code_external>  \n" +
                "  <Data> \n" +
                "    <!--分页信息-->  \n" +
                "    <Pageinfos> \n" +
                "      <Pageinfo> \n" +
                "        <!--分页大小-->  \n" +
                "        <Pagesize>100</Pagesize>  \n" +
                "        <!--页号-->  \n" +
                "        <Pageindex>1</Pageindex>  \n" +
                "        <!--页数-->  \n" +
                "        <Pagecount></Pagecount>  \n" +
                "        <!--总记录数-->  \n" +
                "        <Recordscount></Recordscount> \n" +
                "      </Pageinfo> \n" +
                "    </Pageinfos> \n" +
                "  </Data> \n" +
                "</iihparam>\n"};
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();    // 策略
        httpClientPolicy.setConnectionTimeout( 36000 );    //连接超时
        httpClientPolicy.setAllowChunking( false );
        httpClientPolicy.setReceiveTimeout( 10000 );       //接收超时
        Client client = dcf.createClient(url);
        HTTPConduit http = (HTTPConduit) client.getConduit();
        http.setClient(httpClientPolicy);
        Object[] objects = client.invoke(methodName, param);
        System.out.println(JSON.toJSONString(objects));
    }
}
