package com.pku.smart.vopackage.si0070;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Data")
public class VoReqIIH0070ParamData implements Serializable {
    //<!--患者编码-->
    private String Code_pat = "";//<Code_pat></Code_pat>
    //<!--就诊卡号-->
    private String Code_card = "";//<Code_card></Code_card>
    //<!--身份证号-->
    private String Code_idnum = "";//<Code_idnum></Code_idnum>
    //<!--医院编码-->
    private String Code_hospital = "";//<Code_hospital></Code_hospital>
    //<!--操作员编码-->
    private String Code_opera = "";//<Code_opera></Code_opera>
    //<!--就诊类型-->
    private String Code_enttp = "";//<Code_enttp></Code_enttp>
    //<!--支付方式-->
    private String Code_pm = "";//<Code_pm></Code_pm>
    //<!--交易凭证号-->
    private String Bankno = "";//<Bankno></Bankno>
    //<!--订单流水号-->
    private String Paymodenode = "";//<Paymodenode></Paymodenode>
    //<!--银行卡号-->
    private String Bankcardno = "";//<Bankcardno></Bankcardno>
    //<!--收款金额-->
    private String Amt = "";//<Amt></Amt>
    //<!--终端编码-->
    private String Sd_pttp = "";//<Sd_pttp></Sd_pttp>
    //<!--票据类型-->
    private String Inc_type = "";//<Inc_type></Inc_type>
    //<!--计算机标识-->
    private String Pcid = "";//<Pcid></Pcid>
    //<!--医保产品编码-->
    private String Code_hp = "";//<Code_hp></Code_hp>
    //<!--支付顺序号-->
    private String Payno = "";//<Payno></Payno>
    //<!--门诊收款医保信息-->
    @XStreamAlias("Hpinfos")
    private VoReqIIH0070ParamDataHpinfoList Hpinfos = new VoReqIIH0070ParamDataHpinfoList();
}
