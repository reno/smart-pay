package com.pku.smart.vopackage.si0058;

import com.pku.smart.vopackage.VoIIHParam;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("iihparam")
public class VoReqIIH0058Param extends VoIIHParam implements Serializable {
    @XStreamAlias("Data")
    private VoReqIIH0058ParamData data = new VoReqIIH0058ParamData();
}
