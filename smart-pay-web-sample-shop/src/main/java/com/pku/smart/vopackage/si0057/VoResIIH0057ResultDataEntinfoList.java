package com.pku.smart.vopackage.si0057;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@XStreamAlias("Entinfos")
public class VoResIIH0057ResultDataEntinfoList implements Serializable {
    @XStreamImplicit(itemFieldName = "Entinfo")
    private List<VoResIIH0057ResultDataEntinfo> Entinfo = new ArrayList<>();
}
