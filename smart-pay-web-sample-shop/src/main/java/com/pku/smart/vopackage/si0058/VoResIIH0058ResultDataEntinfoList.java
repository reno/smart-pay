package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@XStreamAlias("Entinfos")
public class VoResIIH0058ResultDataEntinfoList implements Serializable {
    @XStreamImplicit(itemFieldName = "Entinfo")
    private List<VoResIIH0058ResultDataEntinfo> Entinfo;
}
