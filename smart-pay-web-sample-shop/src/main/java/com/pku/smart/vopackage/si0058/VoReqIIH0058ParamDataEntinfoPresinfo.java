package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Presinfo")
public class VoReqIIH0058ParamDataEntinfoPresinfo implements Serializable {
    //<!--申请单编号-->
    private String Code_apply = "";//<Code_apply>2000000018-R0000000368NN1</Code_apply>
    //<!--医嘱类别-->
    private String Sd_srvtp = "";//<Sd_srvtp>01</Sd_srvtp>
    //<!--医嘱名称-->
    private String Name_or = "";//<Name_or></Name_or>
    //<!--开单日期时间-->
    private String Dt_effe = "";//<Dt_effe></Dt_effe>
    //<!--开单科室编码-->
    private String Code_dep_or = "";//<Code_dep_or>100101</Code_dep_or>
    //<!--开单科室名称-->
    private String Name_dep_or = "";//<Name_dep_or></Name_dep_or>
    //<!--开单医生编号-->
    private String Code_emp_or = "";//<Code_emp_or>00140</Code_emp_or>
    //<!--开单医生名称-->
    private String Name_emp_or = "";//<Name_emp_or></Name_emp_or>
    //<!--处方金额-->
    private String Amt_pres = "";//<Amt_pres></Amt_pres>
    //<!--门诊划价出参处方信息-->
    @XStreamAlias("Presitms")
    private VoReqIIH0058ParamDataEntinfoPresinfoPresitmList Presitms = new VoReqIIH0058ParamDataEntinfoPresinfoPresitmList();
}
