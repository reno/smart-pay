package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Presitm")
public class VoReqIIH0058ParamDataEntinfoPresinfoPresitm implements Serializable {
    //<!--记账流水号-->
    private String Code_cg = "";//<Code_cg></Code_cg>
    //<!--服务编码-->
    private String Code_srv = "";//<Code_srv>YP000002</Code_srv>
    //<!--服务名称-->
    private String Name_srv = "";//<Name_srv></Name_srv>
    //<!--服务单位名称-->
    private String Name_srvu = "";//<Name_srvu></Name_srvu>
    //<!--规格-->
    private String Spec = "";//<Spec></Spec>
    //<!--单价-->
    private String Price = "";//<Price>253</Price>
    //<!--数量-->
    private String Quan = "";//<Quan>3</Quan>
    //<!--账单项编码-->
    private String Code_inccaitm = "";//<Code_inccaitm></Code_inccaitm>
    //<!--账单项名称-->
    private String Name_inccaitm = "";//<Name_inccaitm></Name_inccaitm>
    //<!--执行科室编码-->
    private String Code_dep_mp = "";//<Code_dep_mp></Code_dep_mp>
    //<!--执行科室名称-->
    private String Name_dep_mp = "";//<Name_dep_mp></Name_dep_mp>
    //<!--明细金额-->
    private String Amt = "";//<Amt></Amt>
    //<!--开单时间-->
    private String Dt_or = "";//<Dt_or></Dt_or>
}
