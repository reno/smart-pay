package com.pku.smart.vopackage.si0057;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Presinfo")
public class VoResIIH0057ResultDataEntinfoPresinfo implements Serializable {
    private String Code_apply = "";//<Code_apply>2000000018-R0000000368NN1</Code_apply>
    private String Sd_srvtp = "";//<Sd_srvtp>01</Sd_srvtp>
    private String Name_or = "";//<Name_or>0.9%氯化钠注射液</Name_or>
    private String Dt_effe = "";//<Dt_effe>2020-05-18 09:00:00</Dt_effe>
    private String Code_dep_or = "";//<Code_dep_or>100101</Code_dep_or>
    private String Name_dep_or = "";//<Name_dep_or>骨一科门诊</Name_dep_or>
    private String Code_emp_or = "";//<Code_emp_or>00140</Code_emp_or>
    private String Name_emp_or = "";//<Name_emp_or>颜国飞</Name_emp_or>
    private String Amt_pres = "";//<Amt_pres>759</Amt_pres>
    @XStreamAlias("Presitms")
    private VoResIIH0057ResultDataEntinfoPresinfoPresitmList Presitms = new VoResIIH0057ResultDataEntinfoPresinfoPresitmList();
}
