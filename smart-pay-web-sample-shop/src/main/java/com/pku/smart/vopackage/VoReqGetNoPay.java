package com.pku.smart.vopackage;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class VoReqGetNoPay implements Serializable {
    /**
     * ①IIH患者编号
     */
    @NotNull(message = "患者编号[Code_pat]不能为空")
    private String Code_pat;

    private String Times_op;

    private String Code_dep;

    private String Code_apply;
}
