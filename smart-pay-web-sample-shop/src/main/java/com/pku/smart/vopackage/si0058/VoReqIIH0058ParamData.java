package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Data")
public class VoReqIIH0058ParamData implements Serializable {
    //<!--患者编码-->
    private String Code_pat = "";//<Code_pat>100000011500</Code_pat>
    //<!--就诊卡号-->
    private String Code_card = "";//<Code_card></Code_card>
    //<!--身份证号-->
    private String Code_idnum = "";//<Code_idnum></Code_idnum>
    //<!--医院编码-->
    private String Code_hospital = "";//<Code_hospital>1</Code_hospital>
    //<!--就诊类型-->
    private String Code_enttp;//<Code_enttp>01</Code_enttp>
    //<!--操作员编码-->
    private String Code_opera = "";//<Code_opera>00000</Code_opera>
    //<!--医保产品编码-->
    private String Code_hp = "";//<Code_hp></Code_hp>
    //<!--门诊划价就诊信息-->
    @XStreamAlias("Entinfos")
    private VoReqIIH0058ParamDataEntinfoList Entinfos = new VoReqIIH0058ParamDataEntinfoList();
}
