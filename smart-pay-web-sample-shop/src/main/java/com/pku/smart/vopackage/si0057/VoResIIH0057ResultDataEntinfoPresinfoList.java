package com.pku.smart.vopackage.si0057;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@XStreamAlias("Presinfos")
public class VoResIIH0057ResultDataEntinfoPresinfoList implements Serializable {
    @XStreamImplicit(itemFieldName = "Presinfo")
    private List<VoResIIH0057ResultDataEntinfoPresinfo> Presinfo = new ArrayList<>();
}
