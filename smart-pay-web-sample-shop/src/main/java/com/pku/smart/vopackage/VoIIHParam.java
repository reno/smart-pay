package com.pku.smart.vopackage;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("iihparam")
public class VoIIHParam implements Serializable {
    //<!--用户编码-->
    //<Code_user>00000</Code_user>
    @XStreamAlias("Code_user")
    private String Code_user = "";
    //<!--科室编码-->
    //<Code_dep>4022</Code_dep>
    @XStreamAlias("Code_dep")
    private String Code_dep = "";
    //<!--外部编码-->
    //<Code_external>IIH</Code_external>
    @XStreamAlias("Code_external")
    private String Code_external = "";
}
