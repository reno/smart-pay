package com.pku.smart.vopackage.si0070;

import com.pku.smart.vopackage.VoIIHParam;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("iihparam")
public class VoReqIIH0070Param extends VoIIHParam implements Serializable {
    @XStreamAlias("Data")
    private VoReqIIH0070ParamData data = new VoReqIIH0070ParamData();
}
