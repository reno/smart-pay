package com.pku.smart.vopackage.si0057;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Data")
public class VoReqIIH0057ParamData implements Serializable {
    //<!--患者编码-->
    //<Code_pat>100000011500</Code_pat>
    @XStreamAlias("Code_pat")
    private String Code_pat = "";
    //<!--就诊卡号-->
    //<Code_card></Code_card>
    @XStreamAlias("Code_card")
    private String Code_card = "";
    //<!--身份证号-->
    //<Code_idnum></Code_idnum>
    @XStreamAlias("Code_idnum")
    private String Code_idnum = "";
    //<!--门诊就诊次数-->
    //<Times_op></Times_op>
    @XStreamAlias("Times_op")
    private String Times_op = "";
    //<!--医院编码-->
    //<Code_hospital>1</Code_hospital>
    @XStreamAlias("Code_hospital")
    private String Code_hospital = "";
    //<!--就诊类型-->
    //<Code_enttp>01</Code_enttp>
    @XStreamAlias("Code_enttp")
    private String Code_enttp = "";
}
