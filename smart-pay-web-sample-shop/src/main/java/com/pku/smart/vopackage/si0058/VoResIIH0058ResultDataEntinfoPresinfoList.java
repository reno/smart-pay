package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@XStreamAlias("Presinfos")
public class VoResIIH0058ResultDataEntinfoPresinfoList implements Serializable {
    @XStreamImplicit(itemFieldName = "Presinfo")
    private List<VoResIIH0058ResultDataEntinfoPresinfo> Presinfo;
}
