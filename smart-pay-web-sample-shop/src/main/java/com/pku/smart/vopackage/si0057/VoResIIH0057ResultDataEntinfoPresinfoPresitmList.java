package com.pku.smart.vopackage.si0057;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@XStreamAlias("Presitms")
public class VoResIIH0057ResultDataEntinfoPresinfoPresitmList implements Serializable {
    @XStreamImplicit(itemFieldName = "Presitm")
    private List<VoResIIH0057ResultDataEntinfoPresinfoPresitm> Presitm = new ArrayList<>();
}
