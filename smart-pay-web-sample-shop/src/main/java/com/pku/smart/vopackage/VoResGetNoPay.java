package com.pku.smart.vopackage;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class VoResGetNoPay implements Serializable {
    /**
     * ①IIH患者编号
     */
    private String patCode;

    /**
     *
     */
    private String patName;

    /**
     * ④身份证号
     */
    private String sdCode;

    /**
     * 操作员编码
     */
    private String operaCode;

    /**
     * 门诊就诊编码
     */
    private String entCode;

    /**
     * 对应医疗单的号码，包括：处方号、检查号、检验号、医嘱号 可包含：会诊单号、用血单号、手术单号、治疗单号
     */
    private String applyCode;

    /**
     * 使用第三方系统处理金额来源描述
     */
    private String sourceDesc;

    /**
     * 总金额
     */
    private Long entAmout;

    /**
     * 支付顺序号码
     */
    private String payNO;

    private String orName;

    private String deptName;

    private String doctorName;

    private String presAmt;
}
