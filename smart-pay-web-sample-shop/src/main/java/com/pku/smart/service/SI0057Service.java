package com.pku.smart.service;

import com.alibaba.fastjson.JSON;
import com.pku.smart.config.IIHApiConfig;
import com.pku.smart.log.MyLog;
import com.pku.smart.utils.XmlUtils;
import com.pku.smart.vopackage.VoIIHResult;
import com.pku.smart.vopackage.si0057.VoReqIIH0057Param;
import com.pku.smart.vopackage.si0057.VoResIIH0057Result;
import com.pku.smart.vopackage.si0057.VoResIIH0057ResultData;
import com.pku.smart.vopackage.si0057.VoResIIH0057ResultDataEntinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SI0057Service {

    private static final MyLog _log = MyLog.getLog(SI0057Service.class);

    @Autowired
    IIHApiConfig config;

    @Autowired
    SmartPayService service;

    public VoResIIH0057Result process(VoReqIIH0057Param param) {
        VoResIIH0057Result resultData = new VoResIIH0057Result();

        String xml = XmlUtils.toXml(param);
        _log.info("参数解析到XML：" + xml);
        String methodName = config.getMethod();
        String[] params = new String[]{"SI0057", xml};

        try {
            Object[] objects = service.invoke(methodName, params);
            _log.info("远程调用返回：" + JSON.toJSONString(objects));
            xml = (String) objects[0];
            Class[] clacks = new Class[]{VoIIHResult.class, VoResIIH0057Result.class, VoResIIH0057ResultData.class, VoResIIH0057ResultDataEntinfo.class};
            resultData = XmlUtils.parseFromXml(clacks, xml);
        } catch (Exception e) {
            e.printStackTrace();
            resultData.setCode("1001");
            resultData.setMsg("远程服务调用失败：" + e.getMessage());
        }

        return resultData;
    }
}
