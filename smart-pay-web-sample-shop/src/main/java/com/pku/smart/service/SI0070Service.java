package com.pku.smart.service;

import com.alibaba.fastjson.JSON;
import com.pku.smart.config.IIHApiConfig;
import com.pku.smart.log.MyLog;
import com.pku.smart.utils.XmlUtils;
import com.pku.smart.vopackage.VoIIHResult;
import com.pku.smart.vopackage.si0058.VoReqIIH0058ParamData;
import com.pku.smart.vopackage.si0058.VoResIIH0058Result;
import com.pku.smart.vopackage.si0058.VoResIIH0058ResultDataEntinfo;
import com.pku.smart.vopackage.si0058.VoResIIH0058ResultDataEntinfoPresinfo;
import com.pku.smart.vopackage.si0070.VoReqIIH0070Param;
import com.pku.smart.vopackage.si0070.VoReqIIH0070ParamDataHpinfo;
import com.pku.smart.vopackage.si0070.VoResIIH0070Result;
import com.pku.smart.vopackage.si0070.VoResIIH0070ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SI0070Service {
    private static final MyLog _log = MyLog.getLog(SI0070Service.class);

    @Autowired
    IIHApiConfig config;

    @Autowired
    SmartPayService service;

    public VoResIIH0070Result process(VoResIIH0058Result param0058) {
        VoResIIH0070Result resultData = new VoResIIH0070Result();

        VoReqIIH0058ParamData patData = param0058.getDataReq();
        VoResIIH0058ResultDataEntinfo entinfo = param0058.getData().getEntinfos().getEntinfo().get(0);
        List<VoResIIH0058ResultDataEntinfoPresinfo> presinfoList = entinfo.getPresinfos().getPresinfo();

        VoReqIIH0070Param param = new VoReqIIH0070Param();
        param.setCode_user(config.getCode_user());
        param.setCode_dep(presinfoList.get(0).getCode_dep_or());
        param.setCode_external(config.getCode_external());
        //<!--患者编码-->
        param.getData().setCode_pat(patData.getCode_pat());//<Code_pat>100000011500</Code_pat>
        //<!--就诊卡号-->
        //<Code_card></Code_card>
        //<!--身份证号-->
        //<Code_idnum></Code_idnum>
        //<!--医院编码-->
        param.getData().setCode_hospital(config.getCode_hospital());//<Code_hospital>1</Code_hospital>
        //<!--操作员编码-->
        param.getData().setCode_opera(config.getCode_user());//<Code_opera>00000</Code_opera>
        //<!--就诊类型-->
        param.getData().setCode_enttp(config.getCode_enttp());//<Code_enttp>01</Code_enttp>
        //<!--支付方式-->
        param.getData().setCode_pm("11");//<Code_pm>01</Code_pm>
        //<!--交易凭证号-->
        param.getData().setBankno(param0058.getData().getPayno());//<Bankno></Bankno>
        //<!--订单流水号-->
        param.getData().setPaymodenode(param0058.getData().getPayno());//<Paymodenode></Paymodenode>
        //<!--银行卡号-->
        //<Bankcardno></Bankcardno>
        //<!--收款金额-->
        param.getData().setAmt(patData.getEntinfos().getEntinfos().get(0).getAmt_ent());//<Amt>759</Amt>
        //<!--终端编码-->
        param.getData().setSd_pttp("06");//<Sd_pttp>06</Sd_pttp>
        //<!--票据类型-->
        param.getData().setInc_type("01");//<Inc_type>01</Inc_type>
        //<!--计算机标识-->
        param.getData().setPcid("01");//<Pcid></Pcid>
        //<!--医保产品编码-->
        //<Code_hp></Code_hp>
        //<!--支付顺序号-->
        param.getData().setPayno(param0058.getData().getPayno());//<Payno>1001Z81000000003692D</Payno>
        param.getData().getHpinfos().getEntinfos().add(new VoReqIIH0070ParamDataHpinfo());

        _log.info("组织参数：" + JSON.toJSONString(param));
        String xml = XmlUtils.toXml(param);
        _log.info("转换为XML：" + xml);
        String methodName = config.getMethod();
        String[] params = new String[]{"SI0058", xml};

        try {
            Object[] objects = service.invoke(methodName, params);
            _log.info("远程调用返回：" + JSON.toJSONString(objects));
            xml = (String) objects[0];
            Class[] clacks = new Class[]{VoIIHResult.class, VoResIIH0070Result.class, VoResIIH0070ResultData.class,};
            resultData = XmlUtils.parseFromXml(clacks, xml);
        } catch (Exception e) {
            e.printStackTrace();
            resultData.setCode("1001");
            resultData.setMsg("远程服务调用失败：" + e.getMessage());
        }

        return resultData;
    }
}
