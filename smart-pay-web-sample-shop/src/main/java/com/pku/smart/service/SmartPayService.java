package com.pku.smart.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.pku.smart.config.IIHApiConfig;
import com.pku.smart.log.MyLog;
import com.pku.smart.trade.exception.BizTradeException;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;

import java.util.Map;

@Service
public class SmartPayService {

    private static final MyLog _log = MyLog.getLog(SmartPayService.class);

    @Autowired
    IIHApiConfig config;

    @Autowired
    Validator validator;

    /**
     * 调用远程服务
     * @param methodName
     * @param params
     * @return
     * @throws Exception
     */
    public Object[] invoke(String methodName,String[] params) throws Exception {
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();    // 策略
        httpClientPolicy.setConnectionTimeout(36000);    //连接超时
        httpClientPolicy.setAllowChunking(false);
        httpClientPolicy.setReceiveTimeout(10000);       //接收超时
        Client client = dcf.createClient(config.getUrl());
        HTTPConduit http = (HTTPConduit) client.getConduit();
        http.setClient(httpClientPolicy);
        Object[] objects = client.invoke(methodName, params);
        System.out.println(JSON.toJSONString(objects));
        return objects;
    }

    /**
     * 参数校验、签名验证
     * @param object
     * @param bindingResult
     */
    public void validateParams(Object object, BindingResult bindingResult){
        validator.validate(object, bindingResult);
        if (bindingResult.hasErrors()) {
            String errorResponse = getErrorResponse(bindingResult);
            _log.error("请求参数异常:{}", errorResponse);
            throw new BizTradeException(BizTradeException.REQUEST_PARAM_ERR, errorResponse);
        }

        Object jsonObject = JSONObject.toJSON(object);
        Map<String, Object> jsonParamMap = JSONObject.parseObject(jsonObject.toString(), Map.class);
        _log.info("parseObject:" + jsonParamMap);
    }

    /**
     * 获取错误返回信息
     *
     * @param bindingResult
     * @return
     */
    public String getErrorResponse(BindingResult bindingResult) {
        StringBuffer sb = new StringBuffer();
        for (ObjectError objectError : bindingResult.getAllErrors()) {
            sb.append(objectError.getDefaultMessage()).append(",");
        }
        sb.delete(sb.length() - 1, sb.length());
        return sb.toString();
    }

//    /**
//     * 调用接口getNoPay
//     * @param requestVo
//     * @return
//     */
//    public VoResGetNoPay getNoPay(VoReqGetNoPay requestVo){
//        //模拟数据
//        VoResGetNoPay responseVo = new VoResGetNoPay();
//        responseVo.setPatCode(requestVo.getPatCode());
//        responseVo.setPatName("王小三");
//        //responseVo.setSdCode(requestVo.getSdCode());
//        //responseVo.setApplyCode(requestVo.getApplyCode());
//        //responseVo.setEntCode(requestVo.getEntCode());
//        responseVo.setEntAmout((long) 100);
//
//        List<VoEntPresInfo> presInfoList = new ArrayList<>();
//        VoEntPresInfo presInfo01 = new VoEntPresInfo();
//        presInfo01.setApplyCode("001");
//        presInfo01.setDeptName("内科");
//        presInfo01.setDoctorName("王小二");
//        presInfo01.setOrName("血常规");
//        presInfo01.setPresAmt((long) 121);
//        presInfo01.setDtEffe(new Date());
//        presInfoList.add(presInfo01);
//
//        VoEntPresInfo presInfo02 = new VoEntPresInfo();
//        presInfo02.setApplyCode("002");
//        presInfo02.setDeptName("内科");
//        presInfo02.setDoctorName("王小二");
//        presInfo02.setOrName("血小板分析");
//        presInfo02.setPresAmt((long) 11);
//        presInfo02.setDtEffe(new Date());
//        presInfoList.add(presInfo02);
//
//        VoEntPresInfo presInfo03 = new VoEntPresInfo();
//        presInfo03.setApplyCode("001");
//        presInfo03.setDeptName("内科");
//        presInfo03.setDoctorName("王小二");
//        presInfo03.setOrName("阿莫西林胶囊");
//        presInfo03.setPresAmt((long) 9);
//        presInfo03.setDtEffe(new Date());
//        presInfoList.add(presInfo03);
//
//        VoEntPresInfo presInfo04 = new VoEntPresInfo();
//        presInfo04.setApplyCode("004");
//        presInfo04.setDeptName("内科");
//        presInfo04.setDoctorName("王小二");
//        presInfo04.setOrName("阿卡波糖平");
//        presInfo04.setPresAmt((long) 2113);
//        presInfo04.setDtEffe(new Date());
//        presInfoList.add(presInfo04);
//
//        VoEntPresInfo presInfo05 = new VoEntPresInfo();
//        presInfo05.setApplyCode("005");
//        presInfo05.setDeptName("内科");
//        presInfo05.setDoctorName("王小二");
//        presInfo05.setOrName("一次性注射器");
//        presInfo05.setPresAmt((long) 20);
//        presInfo05.setDtEffe(new Date());
//        presInfoList.add(presInfo05);
//
//        responseVo.setEntPresList(presInfoList);
//        return responseVo;
//    }

}
