package com.pku.smart.base;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 支付入参(公共)
 */
@Deprecated
@Data
public class VoReqPayBase implements Serializable {
    /**
     * 商户编码
     */
    @NotNull(message = "商户编码[mchId]不能为空")
    private String mchId;

    /**
     * 渠道编码
     */
    @NotNull(message = "渠道编码[channelId]不能为空")
    private String channelId;

    /**
     * 渠道商户编码
     */
    private String channelMchId;

    /**
     * 商户订单号
     *
     * @mbggenerated
     */
    @NotNull(message = "商品订单号[mchOrderNo]不能为空")
    private String mchOrderNo;

    /**
     * 支付金额,单位分
     *
     * @mbggenerated
     */
    @NotNull(message = "支付金额[amount]不能为空")
    private Long totalAmount;

    /**
     * 商品标题
     *
     * @mbggenerated
     */
    @NotNull(message = "商品标题[subject]不能为空")
    private String subject;

    /**
     * 商品描述信息
     *
     * @mbggenerated
     */
    private String body;

    /**
     * 签名
     */
    @NotNull(message = "签名[sign]不能为空")
    private String sign;
}
