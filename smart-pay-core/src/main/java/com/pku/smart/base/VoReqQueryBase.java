package com.pku.smart.base;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Deprecated
@Data
public class VoReqQueryBase implements Serializable {

    /**
     * 商户编码
     */
    @NotNull(message = "商户编码[mchId]不能为空")
    private String mchId;

    /**
     * 商户订单号
     *
     * @mbggenerated
     */
    @NotNull(message = "商品订单号[mchOrderNo]不能为空")
    private String mchOrderNo;

    /**
     * 签名
     */
    @NotNull(message = "签名[sign]不能为空")
    private String sign;
}
