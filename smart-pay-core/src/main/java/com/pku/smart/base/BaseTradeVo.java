package com.pku.smart.base;

import lombok.Data;

import java.io.Serializable;

@Deprecated
@Data
public class BaseTradeVo implements Serializable {
    //银行单号
    private String tradeNo;
    //商户单号
    private String outTradeNo;
    //支付金额
    private Long totalAmount;
}
