package com.pku.smart.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseResult implements Serializable {
    /**
     * SUCCESS/FAIL此字段是通信标识，非交易标识，交易是否成功需要查看resCode来判断
     */
    private String retCode;

    /**
     * 返回信息，如非空，为错误原因 签名失败 参数格式校验错误
     */
    //private String retMsg;

    /**
     * 错误码 如 SYSTEMERROR
     */
    private String errCode;

    /**
     * 结果信息描述 如 微信支付内部错误
     */
    private String errCodeDes;

}
