package com.pku.smart.constant;

public class PayConstant {
    //渠道编码
    public final static String PAY_CHANNEL_WX_NATIVE = "WX_NATIVE";                // 微信原生扫码支付
    public final static String PAY_CHANNEL_WX_MICROPAY = "WX_MICROPAY";         // 微信条码支付
    public final static String PAY_CHANNEL_WX_MWEB = "WX_MWEB";					// 微信H5支付
    public final static String PAY_CHANNEL_WX_JSAPI = "WX_JSAPI";					// 微信JSAPI支付
    public final static String PAY_CHANNEL_ALIPAY_QR = "ALIPAY_QR";                // 支付宝当面付之扫码支付
    public final static String PAY_CHANNEL_ALIPAY_BR = "ALIPAY_BR";             // 支付宝条码支付
    public final static String PAY_CHANNEL_ALIPAY_WAP = "ALIPAY_WAP";	    	// 支付宝WAP支付
    public final static String PAY_CHANNEL_ALIPAY_PROVIDER_ID = "2088002560413959";

    //渠道类型
    public final static String CHANNEL_NAME_WX = "WX";                // 渠道名称:微信
    public final static String CHANNEL_NAME_ALIPAY = "ALIPAY";        // 渠道名称:支付宝

    //交易类型
    public static final String TRADE_TYPE_PAY = "SF";   //收费
    public static final String TRADE_TYPE_REFUND = "TF";//退费

    //交易状态
    public final static String TRADE_STATUS_SUCCESS = "SUCCESS"; //成功
    public final static String TRADE_STATUS_FAILED = "FAIL";   //失败
    public final static String TRADE_STATUS_TIMEOUT = "TIMEOUT";   //超时

    public final static String TRADE_STATUS_SYSTEM_ERROR_CODE = "SYSTEM ERROR";
    public final static String TRADE_STATUS_SYSTEM_ERROR_DESC = "系统错误";

    //支付状态
    public final static Integer PAY_STATUS_CLOSED = -3; 	// 退款或关闭
    public final static Integer PAY_STATUS_EXPIRED = -2; 	// 订单过期
    public final static Integer PAY_STATUS_FAILED = -1; 	// 支付失败
    public final static Integer PAY_STATUS_INIT = 0; 		// 初始态
    public final static Integer PAY_STATUS_PAYING = 1; 	    // 支付中
    public final static Integer PAY_STATUS_SUCCESS = 2; 	// 支付成功
    public final static Integer PAY_STATUS_COMPLETE = 3; 	// 业务完成

    //退款状态
    public final static Integer REFUND_STATUS_INIT = 0; 		//未退费：0  退费中：5
    public final static Integer REFUND_STATUS_SUCCESS = 1; 		//退费成功
    public final static Integer REFUND_STATUS_FAILED = 2; 		//退费失败

    //操作员
    public final static String TRADE_DEFAULT_OPERA = "00000";
}
