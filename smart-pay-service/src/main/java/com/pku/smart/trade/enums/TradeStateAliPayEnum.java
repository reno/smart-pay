package com.pku.smart.trade.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 支付宝订单状态枚举类
 */
public enum TradeStateAliPayEnum {

    WAIT_BUYER_PAY("等待买家付款"),
    TRADE_CLOSED("未付款交易超时关闭，或支付完成后全额退款"),
    TRADE_FINISHED("支付完成"),
    TRADE_SUCCESS("支付成功"),
    TRADE_EXCEPYION("交易异常"),
    FAIL("支付失败");

    /**
     * 描述
     */
    private String desc;

    private TradeStateAliPayEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static Map<String, Map<String, Object>> toMap() {
        TradeStateAliPayEnum[] ary = TradeStateAliPayEnum.values();
        Map<String, Map<String, Object>> enumMap = new HashMap<String, Map<String, Object>>();
        for (int num = 0; num < ary.length; num++) {
            Map<String, Object> map = new HashMap<String, Object>();
            String key = ary[num].name();
            map.put("desc", ary[num].getDesc());
            enumMap.put(key, map);
        }
        return enumMap;
    }

    public static List toList() {
        TradeStateAliPayEnum[] ary = TradeStateAliPayEnum.values();
        List list = new ArrayList();
        for (int i = 0; i < ary.length; i++) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("desc", ary[i].getDesc());
            list.add(map);
        }
        return list;
    }

    public static TradeStateAliPayEnum getEnum(String name) {
        TradeStateAliPayEnum[] arry = TradeStateAliPayEnum.values();
        for (int i = 0; i < arry.length; i++) {
            if (arry[i].name().equalsIgnoreCase(name)) {
                return arry[i];
            }
        }
        return null;
    }

    /**
     * 取枚举的json字符串
     *
     * @return
     */
    public static String getJsonStr() {
        TradeStateAliPayEnum[] enums = TradeStateAliPayEnum.values();
        StringBuffer jsonStr = new StringBuffer("[");
        for (TradeStateAliPayEnum senum : enums) {
            if (!"[".equals(jsonStr.toString())) {
                jsonStr.append(",");
            }
            jsonStr.append("{id:'").append(senum).append("',desc:'").append(senum.getDesc()).append("'}");
        }
        jsonStr.append("]");
        return jsonStr.toString();
    }
}
