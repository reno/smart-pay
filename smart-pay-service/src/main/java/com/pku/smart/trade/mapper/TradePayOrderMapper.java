package com.pku.smart.trade.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pku.smart.trade.entity.TradePayOrder;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface TradePayOrderMapper extends BaseMapper<TradePayOrder> {
}
