package com.pku.smart.trade.service;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.pku.smart.trade.entity.TradePayOrder;

public interface ITradePayOrderService {

    /**
     * 根据渠道订单号查询
     * @param channelOrderNo
     * @return
     */
    TradePayOrder getTradePayOrder(String channelOrderNo);

    /**
     * 根据商户编码和商户订单号查询支付订单
     * @param mchId
     * @param mchOrderNo
     * @return
     */
    TradePayOrder getTradePayOrder(String mchId,String mchOrderNo);

    /**
     * 查询支付订单 商户订单和渠道订单二选一
     * @param mchId
     * @param mchOrderNo
     * @param channelOrderNo
     * @return
     */
    TradePayOrder getTradePayOrder(String mchId,String mchOrderNo,String channelOrderNo);

    /**
     * 插入支付订单
     * @param tradePayOrder
     * @return
     */
    int insertTradePayOrder(TradePayOrder tradePayOrder);

    /**
     * 更新
     * @param tradePayOrder
     * @return
     */
    int updateTradePayOrder(TradePayOrder tradePayOrder);

    int updateTradePayOrder(LambdaUpdateWrapper<TradePayOrder> updateWrapper);
}
