package com.pku.smart.trade.vopackage;

import com.pku.smart.base.VoResBase;
import lombok.Data;

import java.io.Serializable;

@Data
public class VoResPayScan extends VoResBase implements Serializable {
    private String mchId;

    private String channelId;

    private String mchOrderNo;

    private String channelOrderNo;

    private String codeUrl;
}
