package com.pku.smart.trade.vopackage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@ApiModel(value = "VoReqPayScan",description = "扫码支付入参")
public class VoReqPayScan implements Serializable {

    /**
     * 商户编码
     */
    @ApiModelProperty(value = "商户编码", required = true, example = "10000000")
    @NotNull(message = "商户编码[mchId]不能为空")
    private String mchId;

    /**
     * 渠道编码
     */
    @ApiModelProperty(value = "渠道编码", required = true, allowableValues = "ALIPAY_QR,WX_NATIVE", example = "ALIPAY_QR")
    @NotNull(message = "渠道编码[channelId]不能为空")
    private String channelId;

    /**
     * 渠道商户编码
     */
    //private String channelMchId;

    /**
     * 商户订单号
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "商品订单号", required = true)
    @NotNull(message = "商品订单号[mchOrderNo]不能为空")
    private String mchOrderNo;

    /**
     * 支付金额,单位分
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "支付金额", required = true)
    @NotNull(message = "支付金额[amount]不能为空")
    private Long totalAmount;

    /**
     * 商品标题
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "商品标题", required = true)
    @NotNull(message = "商品标题[subject]不能为空")
    private String subject;

    /**
     * 商品描述信息
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "商品描述信息")
    private String body;

    /**
     * 商户回调地址
     */
    @ApiModelProperty(value = "商户回调地址")
    private String notifyUrl;

    /**
     * 客户端IP
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "订单IP", required = true)
    @Size(min = 1 ,  max = 20 , message = "订单IP[clientIp]长度最小1位，最大20位")
    @NotNull(message = "订单IP[clientIp]不能为空")
    private String clientIp;

    /**
     *
     */
    @ApiModelProperty(value = "设备号")
    private String device;

    /**
     * 特定渠道发起时额外参数
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "特定渠道发起时额外参数")
    private String extra;

    /**
     * 签名
     */
    @ApiModelProperty(value = "签名", required = true)
    @NotNull(message = "签名[sign]不能为空")
    private String sign;
}
