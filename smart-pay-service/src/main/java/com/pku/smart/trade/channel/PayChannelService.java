package com.pku.smart.trade.channel;

import com.alibaba.fastjson.JSONObject;
import com.pku.smart.trade.vopackage.*;

public interface PayChannelService {

    /**
     * 初始化配置
     * @param channelId
     */
    void init(String mchId, String channelId);

    /**
     * 条码支付接口
     * @param requestVo
     * @return
     */
    JSONObject pay(VoReqPayCode requestVo);

    /**
     * 三码支付
     * @param requestVo
     * @return
     */
    JSONObject precreate(VoReqPayScan requestVo);

    /**
     * 查询条码支付结果
     * @param requestVo
     * @return
     */
    JSONObject query(VoReqQueryPay requestVo);

    /**
     * 退款
     * @param requestVo
     * @return
     */
    JSONObject refund(VoReqRefund requestVo);

    /**
     * 退款查询
     * @param requestVo
     * @return
     */
    JSONObject queryRefund(VoReqQueryRefund requestVo);

    /**
     * 撤销
     * @param requestVo
     * @return
     */
    JSONObject reverse(VoReqReverse requestVo);

    /**
     * 手机网站支付
     * @param requestVo
     * @return
     */
    JSONObject wappay(VoReqPayWap requestVo);

    /**
     * 对账单下载
     * @param requestVo
     * @return
     */
    JSONObject downbill(VoReqDownBill requestVo);
}
