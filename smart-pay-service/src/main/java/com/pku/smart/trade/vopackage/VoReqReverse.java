package com.pku.smart.trade.vopackage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value = "VoReqReverse",description = "撤销入参")
public class VoReqReverse implements Serializable {

    /**
     * 商户编码
     */
    @ApiModelProperty(value = "商户编码", required = true, example = "10000000")
    @NotNull(message = "商户编码[mchId]不能为空")
    private String mchId;

    /**
     * 商户订单号
     */
    @ApiModelProperty(value = "商品订单号", required = true)
    @NotNull(message = "商户订单号[mchOrderNo]不能为空")
    private String mchOrderNo;

    /**
     * 渠道订单号
     */
    @ApiModelProperty(value = "渠道订单号")
    private String channelOrderNo;

    /**
     * 签名
     */
    @ApiModelProperty(value = "签名", required = true)
    @NotNull(message = "签名[sign]不能为空")
    private String sign;
}
