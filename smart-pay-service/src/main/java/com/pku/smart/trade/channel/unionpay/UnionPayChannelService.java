package com.pku.smart.trade.channel.unionpay;

import com.alibaba.fastjson.JSONObject;
import com.pku.smart.trade.channel.PayChannelService;
import com.pku.smart.trade.vopackage.*;
import org.springframework.stereotype.Service;

@Service
public class UnionPayChannelService implements PayChannelService {

    /**
     * 初始化配置
     *
     * @param mchId
     * @param channelId
     */
    @Override
    public void init(String mchId, String channelId) {

    }

    /**
     * 条码支付接口
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject pay(VoReqPayCode requestVo) {
        return null;
    }

    /**
     * 三码支付
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject precreate(VoReqPayScan requestVo) {
        return null;
    }

    /**
     * 查询条码支付结果
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject query(VoReqQueryPay requestVo) {
        return null;
    }

    /**
     * 退款
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject refund(VoReqRefund requestVo) {
        return null;
    }

    /**
     * 退款查询
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject queryRefund(VoReqQueryRefund requestVo) {
        return null;
    }

    /**
     * 撤销
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject reverse(VoReqReverse requestVo) {
        return null;
    }

    /**
     * 手机网站支付
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject wappay(VoReqPayWap requestVo) {
        return null;
    }

    /**
     * 对账单下载
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject downbill(VoReqDownBill requestVo) {


        return null;
    }
}
