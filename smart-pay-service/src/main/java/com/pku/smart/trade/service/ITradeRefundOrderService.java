package com.pku.smart.trade.service;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.pku.smart.trade.entity.TradeRefundOrder;

public interface ITradeRefundOrderService {

    /**
     * 查询商户下退款单号
     * @param mchId
     * @param mchRefundNo
     * @return
     */
    TradeRefundOrder getTradeRefundOrder(String mchId, String mchRefundNo);

    /***
     * 根据商户订单号和退款订单号查询
     * @param mchId
     * @param mchOrderNo
     * @param mchRefundNo
     * @return
     */
    TradeRefundOrder getTradeRefundOrder(String mchId, String mchOrderNo, String mchRefundNo);

    /**
     * 更新退款订单
     * @param refundOrder
     * @return
     */
    int insertTradeRefundOrder(TradeRefundOrder refundOrder);

    /**
     * 更新退款订单
     * @param refundOrder
     * @return
     */
    int updateTradeRefundOrder(TradeRefundOrder refundOrder);

    /**
     * 更新退款订单
     * @param updateWrapper
     * @return
     */
    int updateTradeRefundOrder(LambdaUpdateWrapper<TradeRefundOrder> updateWrapper);
}
