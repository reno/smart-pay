package com.pku.smart.trade.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pku.smart.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * 支付记录实体类
 */
@Deprecated
@Data
@TableName(value = "t_pay_record")
public class TradePayRecord extends BaseEntity implements Serializable {
    /**
     * 商户ID
     *
     * @mbggenerated
     */
    @TableField(value = "mch_id")
    private String mchId;

    /**
     * 商户订单号
     *
     * @mbggenerated
     */
    @TableField(value = "mch_order_no")
    private String mchOrderNo;

    /**
     * 支付金额,单位分
     *
     * @mbggenerated
     */
    @TableField(value = "amount")
    private Long amount;

    /**
     * 商品标题
     *
     * @mbggenerated
     */
    @TableField(value = "subject")
    private String subject;

    /**
     * 商品描述信息
     *
     * @mbggenerated
     */
    @TableField(value = "body")
    private String body;

    /**
     * 渠道订单号
     *
     * @mbggenerated
     */
    @TableField(value = "channel_order_no")
    private String channelOrderNo;
}
