package com.pku.smart.trade.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.log.MyLog;
import com.pku.smart.queue.vopackage.VoPollingPay;
import com.pku.smart.trade.channel.PayChannelFactory;
import com.pku.smart.trade.channel.PayChannelService;
import com.pku.smart.trade.entity.TradePayOrder;
import com.pku.smart.trade.entity.TradeRefundOrder;
import com.pku.smart.trade.enums.TradePayRefundStatusEnum;
import com.pku.smart.trade.enums.TradePayStatusEnum;
import com.pku.smart.trade.exception.BizTradeException;
import com.pku.smart.trade.service.IPaymentManagerService;
import com.pku.smart.trade.service.ITradePayOrderService;
import com.pku.smart.trade.service.ITradeRefundOrderService;
import com.pku.smart.trade.service.PaymentBaserService;
import com.pku.smart.trade.vopackage.*;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class PaymentManagerServiceImpl extends PaymentBaserService implements IPaymentManagerService {

    private static final MyLog _log = MyLog.getLog(PaymentManagerServiceImpl.class);

    private PayChannelFactory factory = new PayChannelFactory();

    @Autowired
    ITradePayOrderService tradePayOrderService;

    @Autowired
    ITradeRefundOrderService tradeRefundOrderService;

    /**
     * 条码支付，对应支付宝的条码支付或者微信的刷卡支付
     *
     * @param requestVo
     * @return
     */
    @Override
    public VoResTradePayment codePay(VoReqPayCode requestVo) {
        _log.info("构建支付渠道");
        PayChannelService service = factory.build(requestVo.getMchId(), requestVo.getChannelId());

        _log.info("查询订单");
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(requestVo.getMchId(), requestVo.getMchOrderNo());
        _log.info("判断订单状态 不存在：插入  存在：是否已支付");
        if (tradePayOrder == null) {
            if (InsertNewTradePayOrder(requestVo) != 1) {
                _log.error("付款码支付订单插入失败");
                throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "付款码支付订单插入失败");
            }
        } else {
            if (requestVo.getTotalAmount().equals(tradePayOrder.getAmount())) {
                _log.error("订单已存在，订单金额与传入金额不相等");
                throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "错误的订单金额");
            }
            if (tradePayOrder.getStatus() >= TradePayStatusEnum.PAY_STATUS_SUCCESS.getCode()) {
                _log.error("订单已存在，且订单状态为支付成功");
                throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "订单已支付成功,无需重复支付");
            }
        }

        VoResPayCode responseVo = new VoResPayCode();
        responseVo.setMchId(requestVo.getMchId());
        responseVo.setChannelId(requestVo.getChannelId());
        responseVo.setMchOrderNo(requestVo.getMchOrderNo());

        _log.info("渠道执行");
        Map tradeMap = service.pay(requestVo);

        VoResTradePayment tradeResult = new VoResTradePayment();
        responseVo.setErrCode((String) tradeMap.get("errCode"));
        responseVo.setErrCodeDes((String) tradeMap.get("errMsg"));
        responseVo.setInvokeStatus(String.valueOf(tradeMap.get("tradeStatus")));
        String retCode = (String) tradeMap.getOrDefault("retCode", "");
        _log.info("渠道执行状态：" + retCode);
        if (PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase(retCode)) {
            String channelOrderNo = String.valueOf(tradeMap.get("channelOrderNo"));
            responseVo.setChannelOrderNo(channelOrderNo);
            _log.info("渠道订单号{}(非空标识成功)", channelOrderNo);
            if (StringUtils.isNotBlank(channelOrderNo)) {
                _log.info("支付成功，更新订单表。" + channelOrderNo);
                completeSuccessOrder(responseVo);
            }
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_SUCCESS);
        } else {
            _log.info("渠道执行结果未知");
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            _log.warn("推送到沦陷队列处理");
            VoPollingPay pollingPay = new VoPollingPay();
            pollingPay.setMchId(requestVo.getMchId());
            pollingPay.setChannelId(requestVo.getChannelId());
            pollingPay.setMchOrderNo(requestVo.getMchOrderNo());
            super.pollingPay(pollingPay);
        }
        tradeResult.setRetData(responseVo);
        _log.info("打印：" + JSON.toJSONString(tradeResult));
        return tradeResult;

        //_log.info("执行结果转入上层分析");
        //return super.getPayResultMap(tradeMap,responseVo);
    }

    /**
     * 扫码支付，用户拿手机扫描二维码
     *
     * @param requestVo
     * @return
     */
    @Override
    public VoResTradePayment scanPay(VoReqPayScan requestVo) {
        _log.info("构建支付渠道");
        PayChannelService service = factory.build(requestVo.getMchId(), requestVo.getChannelId());

        _log.info("查询订单");
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(requestVo.getMchId(), requestVo.getMchOrderNo());
        _log.info("判断订单是否存在");
        if (tradePayOrder != null) {
            _log.error("订单已存在，请重新下单");
            throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "订单已存在");
        }

        if (InsertNewTradePayOrder(requestVo) != 1) {
            _log.error("扫码支付订单插入失败");
            throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "扫码支付订单插入失败");
        }

        VoResPayScan responseVo = new VoResPayScan();
        responseVo.setMchId(requestVo.getMchId());
        responseVo.setChannelId(requestVo.getChannelId());
        responseVo.setMchOrderNo(requestVo.getMchOrderNo());

        _log.info("渠道执行");
        Map tradeMap = service.precreate(requestVo);

        _log.info("执行结果转入上层分析");
        return super.getPreCreateResultMap(tradeMap, responseVo);
    }

    /**
     * 查询支付订单
     *
     * @param requestVo
     * @return
     */
    @Override
    public VoResTradePayment payQuery(VoReqQueryPay requestVo) {
        _log.info("根据渠道订单号查询");
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(requestVo.getMchId(), requestVo.getMchOrderNo(), requestVo.getChannelOrderNo());
        if (tradePayOrder == null) {
            _log.error("订单不存在>>>" + requestVo.getMchOrderNo() + ">>>" + requestVo.getChannelOrderNo());
            throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "订单不存在:" + requestVo.getMchOrderNo());
        }

        _log.info("查询-构建支付渠道");
        PayChannelService service = factory.build(requestVo.getMchId(), tradePayOrder.getChannelId());

        String mchOrderNo = tradePayOrder.getMchOrderNo();//二选一 所以这里重新赋值用一下
        String channelOrderNo = tradePayOrder.getChannelOrderNo();

        VoResQueryPay responseVo = new VoResQueryPay();
        responseVo.setMchId(requestVo.getMchId());
        responseVo.setChannelId(tradePayOrder.getChannelId());
        responseVo.setMchOrderNo(mchOrderNo);
        responseVo.setChannelOrderNo(channelOrderNo);

        _log.info("渠道执行");
        Map tradeMap = service.query(requestVo);

        _log.info("执行结果转入上层分析");
        return super.getQueryResultMap(tradeMap, responseVo);
    }

    /**
     * 退款
     *
     * @param requestVo
     * @return
     */
    @Override
    public VoResTradePayment refund(VoReqRefund requestVo) {
        //根据渠道订单号查询
        String mchId = requestVo.getMchId();
        String channelOrderNo = requestVo.getChannelOrderNo();
        String mchOrderNo = requestVo.getMchOrderNo();
        String mchRefundNo = requestVo.getMchRefundNo();
        Long refundAmount = requestVo.getRefundAmount();
        Long totalAmount = requestVo.getTotalAmount();
        if (totalAmount != null) {
            _log.info("当传入原支付金额不为空，则比较一下退款金额");
            if (refundAmount.longValue() > totalAmount.longValue()) {
                _log.error("退款金额传入有误，请核查。" + refundAmount);
                throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "退款金额不能大于原支付金额");
            }
        }
        _log.info("查询对应的支付订单");
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(mchId, mchOrderNo, channelOrderNo);
        if (tradePayOrder == null) {
            _log.error("支付订单不存在，请核查：" + mchOrderNo);
            throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "支付订单不存在，请核查：" + mchOrderNo);
        }
        String channelId = tradePayOrder.getChannelId();
        if (refundAmount.longValue() > tradePayOrder.getAmount().longValue()) {
            _log.error("退款金额不能大于支付金额");
            throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "退款金额不能大于支付金额");
        }
        if (totalAmount == null) {
            totalAmount = tradePayOrder.getAmount();
            requestVo.setTotalAmount(totalAmount);
        }
        if (StringUtils.isBlank(requestVo.getChannelId())) {
            requestVo.setChannelId(channelId);
        }

        _log.info("构建支付渠道");
        PayChannelService service = factory.build(requestVo.getMchId(), channelId);

        _log.info("查询订单");
        TradeRefundOrder tradeRefundOrder = tradeRefundOrderService.getTradeRefundOrder(mchId, mchRefundNo);
        if (tradeRefundOrder == null) {
            tradeRefundOrder = new TradeRefundOrder();
            tradeRefundOrder.setMchRefundNo(mchRefundNo);
            tradeRefundOrder.setMchId(mchId);
            tradeRefundOrder.setMchOrderdNo(mchOrderNo);
            tradeRefundOrder.setChannelId(tradePayOrder.getChannelId());
            tradeRefundOrder.setChannelPayOrderNo(tradePayOrder.getChannelOrderNo());
            tradeRefundOrder.setPayAmount(tradePayOrder.getAmount());
            tradeRefundOrder.setRefundAmount(refundAmount);
            tradeRefundOrder.setCurrency("cny");
            tradeRefundOrder.setStatus(PayConstant.REFUND_STATUS_INIT);
            tradeRefundOrder.setResult(TradePayRefundStatusEnum.REFUND_STATUS_INIT.name());
            tradeRefundOrder.setClientIp(tradePayOrder.getClientIp());
            tradeRefundOrder.setDevice(tradePayOrder.getDevice());
            tradeRefundOrder.setExtra(tradePayOrder.getExtra());
            tradeRefundOrder.setNotifyUrl(tradePayOrder.getNotifyUrl());
            tradeRefundOrder.setCreateTime(new Date());
            tradeRefundOrder.setCreateBy(PayConstant.TRADE_DEFAULT_OPERA);
            tradeRefundOrder.setRefundCreateTime(tradeRefundOrder.getCreateTime());
            tradeRefundOrder.setRemarkInfo("订单退费");
            tradeRefundOrderService.insertTradeRefundOrder(tradeRefundOrder);
        } else {
            if (refundAmount.longValue() <= 0) {
                _log.error("订单已存在，订单金额与传入金额小于零");
                throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "订单已存在,订单金额与传入金额小于零");
            }
            if (refundAmount.longValue() != tradeRefundOrder.getRefundAmount().longValue()) {
                _log.error("订单已存在，订单金额与传入金额不相等");
                throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "订单已存在，订单金额与传入金额不相等");
            }
            if (tradeRefundOrder.getStatus() >= TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.getCode()) {
                _log.error("订单已存在，且订单状态为退款成功");
                throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "订单已退款成功,无需重复退款");
            }
        }

        VoResRefund responseVo = new VoResRefund();
        responseVo.setMchId(mchId);
        responseVo.setChannelId(channelId);
        responseVo.setMchOrderNo(mchOrderNo);
        responseVo.setChannelOrderNo(channelOrderNo);
        responseVo.setMchRefundNo(mchRefundNo);

        _log.info("渠道执行");
        Map tradeMap = service.refund(requestVo);

        _log.info("执行结果转入上层分析");
        return super.getRefundResultMap(tradeMap, responseVo);
    }

    /**
     * 查询退款订单
     *
     * @param requestVo
     * @return
     */
    @Override
    public VoResTradePayment refundQuery(VoReqQueryRefund requestVo) {
        _log.info("退款前查询对应的支付订单");
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(requestVo.getMchId(), requestVo.getMchOrderNo(), requestVo.getChannelOrderNo());
        if (tradePayOrder == null) {
            _log.error("支付订单不存在，请核查：" + requestVo.getMchOrderNo());
            throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "支付订单不存在，请核查：" + requestVo.getMchOrderNo());
        }
        String channelId = tradePayOrder.getChannelId();

        _log.info("构建支付渠道");
        PayChannelService service = factory.build(requestVo.getMchId(), channelId);

        //根据渠道订单号查询
        String mchId = requestVo.getMchId();
        String mchOrderNo = requestVo.getMchOrderNo();
        String mchRefundNo = requestVo.getMchRefundNo();
        String channelOrderNo = requestVo.getChannelOrderNo();
        String channelRefundOrderNo = requestVo.getChannelRefundOrderNo();

        VoResQueryRefund responseVo = new VoResQueryRefund();
        responseVo.setMchId(mchId);
        responseVo.setChannelId(channelId);
        responseVo.setMchOrderNo(mchOrderNo);
        responseVo.setChannelOrderNo(channelOrderNo);
        responseVo.setMchRefundNo(mchRefundNo);
        responseVo.setChannelRefundNo(channelRefundOrderNo);

        _log.info("渠道执行");
        Map tradeMap = service.queryRefund(requestVo);

        _log.info("执行结果转入上层分析");
        return super.getQueryRefundResultMap(tradeMap, responseVo);
    }

    /**
     * 撤销
     *
     * @param requestVo
     * @return
     */
    @Override
    public VoResTradePayment cancel(VoReqReverse requestVo) {
        //根据渠道订单号查询
        String mchId = requestVo.getMchId();
        String channelOrderNo = requestVo.getChannelOrderNo();
        String mchOrderNo = requestVo.getMchOrderNo();

        _log.info("查询对应的支付订单");
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(mchId, mchOrderNo, channelOrderNo);
        if (tradePayOrder == null) {
            _log.error("支付订单不存在，请核查：" + mchOrderNo);
            throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "支付订单不存在，请核查：" + mchOrderNo);
        }
        String channelId = tradePayOrder.getChannelId();

        _log.info("撤销-构建支付渠道");
        PayChannelService service = factory.build(mchId, channelId);

        _log.info("查询订单");
        TradeRefundOrder tradeRefundOrder = tradeRefundOrderService.getTradeRefundOrder(mchId, mchOrderNo, null);
        if (tradeRefundOrder == null) {
            tradeRefundOrder = new TradeRefundOrder();
            tradeRefundOrder.setMchRefundNo(mchOrderNo.replaceAll("P","C"));
            tradeRefundOrder.setMchId(mchId);
            tradeRefundOrder.setMchOrderdNo(mchOrderNo);
            tradeRefundOrder.setChannelId(tradePayOrder.getChannelId());
            tradeRefundOrder.setChannelPayOrderNo(tradePayOrder.getChannelOrderNo());
            tradeRefundOrder.setPayAmount(tradePayOrder.getAmount());
            tradeRefundOrder.setRefundAmount(tradePayOrder.getAmount());
            tradeRefundOrder.setCurrency("cny");
            tradeRefundOrder.setStatus(PayConstant.REFUND_STATUS_INIT);
            tradeRefundOrder.setResult(TradePayRefundStatusEnum.REFUND_STATUS_INIT.name());
            tradeRefundOrder.setClientIp(tradePayOrder.getClientIp());
            tradeRefundOrder.setDevice(tradePayOrder.getDevice());
            tradeRefundOrder.setExtra(tradePayOrder.getExtra());
            tradeRefundOrder.setNotifyUrl(tradePayOrder.getNotifyUrl());
            tradeRefundOrder.setCreateTime(new Date());
            tradeRefundOrder.setCreateBy(PayConstant.TRADE_DEFAULT_OPERA);
            tradeRefundOrder.setRefundCreateTime(tradeRefundOrder.getCreateTime());
            tradeRefundOrder.setRemarkInfo("订单撤销");
            tradeRefundOrderService.insertTradeRefundOrder(tradeRefundOrder);
        } else {
            if (tradeRefundOrder.getStatus() >= TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.getCode()) {
                _log.error("订单已存在，且订单状态为退款成功");
                throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "订单已退款成功,无需重复退款");
            }
        }

        VoResRefund responseVo = new VoResRefund();
        responseVo.setMchId(mchId);
        responseVo.setChannelId(channelId);
        responseVo.setMchOrderNo(mchOrderNo);
        responseVo.setChannelOrderNo(channelOrderNo);

        _log.info("渠道执行");
        Map tradeMap = service.reverse(requestVo);

        _log.info("执行结果转入上层分析");
        return super.getCancelResultMap(tradeMap, responseVo);
    }

    /**
     * 手机网站支付
     *
     * @param requestVo
     * @return
     */
    @Override
    public VoResTradePayment wapPay(VoReqPayWap requestVo) {
        _log.info("构建支付渠道-WAP");
        PayChannelService service = factory.build(requestVo.getMchId(), requestVo.getChannelId());

        _log.info("查询支付订单");
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(requestVo.getMchId(), requestVo.getMchOrderNo());
        _log.info("判断订单是否存在");
        if (tradePayOrder != null) {
            _log.error("订单已存在，请重新下单");
            throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "WAP支付订单已存在");
        }

        if (InsertNewTradePayOrder(requestVo) != 1) {
            _log.error("WAP支付订单插入失败");
            throw new BizTradeException(BizTradeException.TRADE_ORDER_ERROR, "WAP支付订单插入失败");
        }

        VoResPayWap responseVo = new VoResPayWap();
        responseVo.setMchId(requestVo.getMchId());
        responseVo.setChannelId(requestVo.getChannelId());
        responseVo.setMchOrderNo(requestVo.getMchOrderNo());

        _log.info("渠道执行");
        Map tradeMap = service.wappay(requestVo);

        _log.info("执行结果转入上层分析");
        return super.getWapPayResultMap(tradeMap, responseVo);
    }

    /**
     * 扫码支付新增订单
     *
     * @param requestVo
     * @return
     */
    private int InsertNewTradePayOrder(VoReqPayScan requestVo) {
        TradePayOrder tradePayOrder = new TradePayOrder();
        tradePayOrder.setMchId(requestVo.getMchId());
        tradePayOrder.setMchOrderNo(requestVo.getMchOrderNo());
        tradePayOrder.setPayOrderId(requestVo.getMchOrderNo());
        tradePayOrder.setChannelId(requestVo.getChannelId());
        tradePayOrder.setAmount(requestVo.getTotalAmount());
        tradePayOrder.setCurrency("CNY");
        tradePayOrder.setStatus(PayConstant.PAY_STATUS_INIT);
        tradePayOrder.setInvokeStatus(TradePayStatusEnum.PAY_STATUS_INIT.name());
        tradePayOrder.setClientIp(requestVo.getClientIp());
        tradePayOrder.setDevice(requestVo.getDevice());
        tradePayOrder.setSubject(requestVo.getSubject());
        tradePayOrder.setBody(requestVo.getBody());
        tradePayOrder.setExtra(requestVo.getExtra());
        tradePayOrder.setChannelMchId("");
        tradePayOrder.setNotifyUrl(requestVo.getNotifyUrl());
        tradePayOrder.setNotifyCount(0);
        tradePayOrder.setPayCreateTime(new Date());
        tradePayOrder.setExpireTime(DateUtils.addMinutes(tradePayOrder.getPayCreateTime(),15));//15分钟有效期
        return tradePayOrderService.insertTradePayOrder(tradePayOrder);
    }

    /**
     * 条码支付新增订单
     *
     * @param requestVo
     * @return
     */
    private int InsertNewTradePayOrder(VoReqPayCode requestVo) {
        TradePayOrder tradePayOrder = new TradePayOrder();
        tradePayOrder.setMchId(requestVo.getMchId());
        tradePayOrder.setMchOrderNo(requestVo.getMchOrderNo());
        tradePayOrder.setPayOrderId(requestVo.getMchOrderNo());
        tradePayOrder.setChannelId(requestVo.getChannelId());
        tradePayOrder.setAmount(requestVo.getTotalAmount());
        tradePayOrder.setCurrency("CNY");
        tradePayOrder.setStatus(PayConstant.PAY_STATUS_INIT);
        tradePayOrder.setInvokeStatus(TradePayStatusEnum.PAY_STATUS_INIT.name());
        tradePayOrder.setClientIp(requestVo.getClientIp());
        tradePayOrder.setDevice(requestVo.getDevice());
        tradePayOrder.setSubject(requestVo.getSubject());
        tradePayOrder.setBody(requestVo.getBody());
        tradePayOrder.setExtra(requestVo.getExtra());
        tradePayOrder.setScene(requestVo.getScene());
        tradePayOrder.setAuthCode(requestVo.getAuthCode());
        tradePayOrder.setChannelMchId(" ");
        tradePayOrder.setNotifyUrl(" ");
        tradePayOrder.setNotifyCount(0);
        tradePayOrder.setPayCreateTime(new Date());
        tradePayOrder.setExpireTime(DateUtils.addMinutes(tradePayOrder.getPayCreateTime(),15));//15分钟有效期
        return tradePayOrderService.insertTradePayOrder(tradePayOrder);
    }

    /**
     * 手机网站支付增加订单信息
     *
     * @param requestVo
     * @return
     */
    private int InsertNewTradePayOrder(VoReqPayWap requestVo) {
        TradePayOrder tradePayOrder = new TradePayOrder();
        tradePayOrder.setMchId(requestVo.getMchId());
        tradePayOrder.setMchOrderNo(requestVo.getMchOrderNo());
        tradePayOrder.setPayOrderId(requestVo.getMchOrderNo());
        tradePayOrder.setChannelId(requestVo.getChannelId());
        tradePayOrder.setAmount(requestVo.getTotalAmount());
        tradePayOrder.setCurrency("CNY");
        tradePayOrder.setStatus(PayConstant.PAY_STATUS_INIT);
        tradePayOrder.setInvokeStatus(TradePayStatusEnum.PAY_STATUS_INIT.name());
        tradePayOrder.setClientIp(requestVo.getClientIp());
        tradePayOrder.setDevice(requestVo.getDevice());
        tradePayOrder.setSubject(requestVo.getSubject());
        tradePayOrder.setBody(requestVo.getBody());
        tradePayOrder.setExtra(requestVo.getExtra());
        tradePayOrder.setChannelMchId("");
        tradePayOrder.setNotifyUrl(requestVo.getNotifyUrl());
        tradePayOrder.setNotifyCount(0);
        tradePayOrder.setPayCreateTime(new Date());
        tradePayOrder.setExpireTime(DateUtils.addMinutes(tradePayOrder.getPayCreateTime(),15));//15分钟有效期
        return tradePayOrderService.insertTradePayOrder(tradePayOrder);
    }

    private void completeSuccessOrder(VoResPayCode responseVo) {
        String mchId = responseVo.getMchId();
        String mchOrderNo = responseVo.getMchOrderNo();
        String channelOrderNo = responseVo.getChannelOrderNo();
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(mchId, mchOrderNo);
        LambdaUpdateWrapper<TradePayOrder> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(TradePayOrder::getErrCode, responseVo.getErrCode());
        updateWrapper.set(TradePayOrder::getErrMsg, responseVo.getErrCodeDes());
        updateWrapper.set(TradePayOrder::getStatus, PayConstant.PAY_STATUS_SUCCESS);
        updateWrapper.set(TradePayOrder::getInvokeStatus, TradePayStatusEnum.getObject(PayConstant.PAY_STATUS_SUCCESS).name());
        updateWrapper.set(TradePayOrder::getPaySuccTime, new Date());
        if (StringUtils.isNotBlank(channelOrderNo)) {
            updateWrapper.set(TradePayOrder::getChannelOrderNo, channelOrderNo);
        }
        updateWrapper.eq(TradePayOrder::getId,tradePayOrder.getId());
        updateWrapper.eq(TradePayOrder::getMchId,tradePayOrder.getMchId());
        updateWrapper.eq(TradePayOrder::getMchOrderNo,tradePayOrder.getMchOrderNo());
        tradePayOrderService.updateTradePayOrder(updateWrapper);
        _log.info("订单{}支付成功.", mchOrderNo);
    }
}
