package com.pku.smart.trade.vopackage;

import com.pku.smart.base.VoResBase;
import lombok.Data;

import java.io.Serializable;

@Data
public class VoResDownBill extends VoResBase implements Serializable {

    private String mchId;

    private String channelName;

    private String billDate;

    private Integer billRecords;
}
