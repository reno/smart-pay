package com.pku.smart.trade.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.log.MyLog;
import com.pku.smart.queue.config.PollingConfig;
import com.pku.smart.queue.polling.Mq4PayPolling;
import com.pku.smart.queue.polling.Mq4RefundPolling;
import com.pku.smart.queue.vopackage.VoPollingPay;
import com.pku.smart.queue.vopackage.VoPollingRefund;
import com.pku.smart.trade.entity.TradePayOrder;
import com.pku.smart.trade.entity.TradeRefundOrder;
import com.pku.smart.trade.enums.TradePayRefundStatusEnum;
import com.pku.smart.trade.enums.TradePayStatusEnum;
import com.pku.smart.trade.vopackage.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Map;

public class PaymentBaserService {

    private static final MyLog _log = MyLog.getLog(PaymentBaserService.class);

    @Autowired
    PollingConfig pollingConfig;

    @Autowired
    Mq4PayPolling mq4PayPolling;

    @Autowired
    Mq4RefundPolling mq4RefundPolling;

    @Autowired
    ITradePayOrderService tradePayOrderService;

    @Autowired
    ITradeRefundOrderService tradeRefundOrderService;

    /**
     * 条码支付结果分析
     *
     * @param resultMap
     * @return
     */
    @Deprecated
    public VoResTradePayment getPayResultMap(Map resultMap, VoResPayCode responseVo) {
        VoResTradePayment tradeResult = new VoResTradePayment();

        responseVo.setErrCode((String) resultMap.get("errCode"));
        responseVo.setErrCodeDes((String) resultMap.get("errMsg"));

        String retCode = (String) resultMap.getOrDefault("retCode", "");
        if (PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase(retCode)) {
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_SUCCESS);

            String mchId = responseVo.getMchId();
            String mchOrderNo = responseVo.getMchOrderNo();
            String channelOrderNo = String.valueOf(resultMap.get("channelOrderNo"));
            _log.info("支付成功，更新订单表。" + channelOrderNo);
            completeSuccessOrder(mchId, mchOrderNo, channelOrderNo, responseVo.getErrCode(), responseVo.getErrCodeDes());

            responseVo.setChannelOrderNo(channelOrderNo);
        } else if (PayConstant.TRADE_STATUS_FAILED.equalsIgnoreCase(retCode)) {
            _log.info("失败");
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_FAILED);
        } else if (PayConstant.TRADE_STATUS_TIMEOUT.equalsIgnoreCase(retCode)) {
            _log.info("超时");
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_FAILED);
        }

        tradeResult.setRetData(responseVo);
        _log.info("打印：" + JSON.toJSONString(tradeResult));
        return tradeResult;
    }

    /**
     * 扫码支付结果分析
     *
     * @param resultMap
     * @param responseVo
     * @return
     */
    public VoResTradePayment getPreCreateResultMap(Map resultMap, VoResPayScan responseVo) {
        VoResTradePayment tradeResult = new VoResTradePayment();

        responseVo.setErrCode((String) resultMap.get("errCode"));
        responseVo.setErrCodeDes((String) resultMap.get("errMsg"));

        String retCode = (String) resultMap.getOrDefault("retCode", "");
        if (PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase(retCode)) {
            String mchId = responseVo.getMchId();
            String mchOrderNo = responseVo.getMchOrderNo();
            String codeUrl = String.valueOf(resultMap.get("codeUrl"));
            responseVo.setCodeUrl(codeUrl);
            completePayingOrder(mchId, mchOrderNo, null, codeUrl);
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_SUCCESS);
            _log.warn("二维码支付-推送到沦陷队列处理");
            VoPollingPay pollingPay = new VoPollingPay();
            pollingPay.setMchId(mchId);
            pollingPay.setChannelId(mchOrderNo);
            pollingPay.setMchOrderNo(responseVo.getMchOrderNo());
            this.pollingPay(pollingPay);
        } else {
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            _log.error("二维码没有，不予沦陷");
        }

        tradeResult.setRetData(responseVo);
        _log.info("打印：" + JSON.toJSONString(tradeResult));
        return tradeResult;
    }

    /**
     * 支付订单查询
     *
     * @param resultMap
     * @param responseVo
     * @return
     */
    public VoResTradePayment getQueryResultMap(Map resultMap, VoResQueryPay responseVo) {
        VoResTradePayment tradeResult = new VoResTradePayment();

        responseVo.setErrCode((String) resultMap.get("errCode"));
        responseVo.setErrCodeDes((String) resultMap.get("errMsg"));

        String retCode = (String) resultMap.getOrDefault("retCode", "");
        if (!PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase(retCode)) {
            String mchId = responseVo.getMchId();
            String mchOrderNo = responseVo.getMchOrderNo();
            completeFailedOrder(mchId, mchOrderNo,responseVo.getErrCode(),responseVo.getErrCodeDes(),String.valueOf(resultMap.get("tradeStatus")));
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            tradeResult.setRetData(responseVo);
            return tradeResult;
        }

        String tradeStatus = (String) resultMap.get("tradeStatus");
        String channelOrderNo = (String) resultMap.get("channelOrderNo");
        if (TradePayStatusEnum.PAY_STATUS_SUCCESS.name().equalsIgnoreCase(tradeStatus)) {
            responseVo.setInvokeStatus(TradePayStatusEnum.PAY_STATUS_SUCCESS.name());
            responseVo.setChannelOrderNo(channelOrderNo);
            _log.info("支付成功，更新订单表。" + channelOrderNo);
            String mchId = responseVo.getMchId();
            String mchOrderNo = responseVo.getMchOrderNo();
            completeSuccessOrder(mchId, mchOrderNo, channelOrderNo,responseVo.getErrCode(),responseVo.getErrCodeDes());
        } else if (TradePayStatusEnum.PAY_STATUS_PAYING.name().equalsIgnoreCase(tradeStatus)) {
            responseVo.setInvokeStatus(TradePayStatusEnum.PAY_STATUS_PAYING.name());
            responseVo.setChannelOrderNo(channelOrderNo);
            String mchId = responseVo.getMchId();
            String mchOrderNo = responseVo.getMchOrderNo();
            completePayingOrder(mchId, mchOrderNo, channelOrderNo, null);
            responseVo.setErrCode((String) resultMap.get("errCode"));
            responseVo.setErrCodeDes((String) resultMap.get("errMsg"));
        } else {
            responseVo.setInvokeStatus(TradePayStatusEnum.PAY_STATUS_FAILED.getDesc());
            responseVo.setErrCode((String) resultMap.get("errCode"));
            responseVo.setErrCodeDes((String) resultMap.get("errMsg"));
        }

        tradeResult.setRetCode(PayConstant.TRADE_STATUS_SUCCESS);
        tradeResult.setRetData(responseVo);
        _log.info("打印：" + JSON.toJSONString(tradeResult));

        return tradeResult;
    }

    /**
     * 订单撤销
     *
     * @param resultMap
     * @param responseVo
     * @return 成功or失败
     */
    public VoResTradePayment getCancelResultMap(Map resultMap, VoResRefund responseVo) {
        VoResTradePayment tradeResult = new VoResTradePayment();

        responseVo.setErrCode((String) resultMap.get("errCode"));
        responseVo.setErrCodeDes((String) resultMap.get("errMsg"));

        if (!PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase((String) resultMap.getOrDefault("retCode", ""))) {
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            tradeResult.setRetData(responseVo);
            _log.warn("撤销推送到沦陷队列处理");
            VoPollingRefund pollingRefund = new VoPollingRefund();
            pollingRefund.setMchId(responseVo.getMchId());
            pollingRefund.setChannelId(responseVo.getChannelId());
            pollingRefund.setMchOrderNo(responseVo.getMchOrderNo());
            pollingRefund.setMchRefundNo(responseVo.getMchRefundNo());
            this.pollingRefund(pollingRefund);
        } else {
            responseVo.setChannelRefundNo(String.valueOf(resultMap.get("channelRefundOrderNo")));
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_SUCCESS);
            completeSuccessRefundOrder(responseVo.getMchId(),responseVo.getMchOrderNo(),responseVo.getChannelRefundNo(),responseVo.getErrCode(),responseVo.getErrCodeDes());
            _log.info("撤销订单更新成功");
        }

        tradeResult.setRetData(responseVo);
        _log.info("打印：" + JSON.toJSONString(tradeResult));
        return tradeResult;
    }

    /**
     * 退款查询
     *
     * @param resultMap
     * @param responseVo
     * @return
     */
    public VoResTradePayment getQueryRefundResultMap(Map resultMap, VoResQueryRefund responseVo) {
        VoResTradePayment tradeResult = new VoResTradePayment();

        responseVo.setErrCode((String) resultMap.get("errCode"));
        responseVo.setErrCodeDes((String) resultMap.get("errMsg"));

        if (!PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase((String) resultMap.getOrDefault("retCode", ""))) {
            _log.error("执行退款查询返回失败了");
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            tradeResult.setRetData(responseVo);
            return tradeResult;
        }

        String tradeStatus = (String) resultMap.get("tradeStatus");
        _log.info("退款交易返回tradeStatus=" + tradeStatus);
        if (TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.name().equalsIgnoreCase(tradeStatus)) {
            String channelOrderNo = responseVo.getChannelOrderNo();
            responseVo.setInvokeStatus(TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.getCode().toString());
            responseVo.setChannelOrderNo(channelOrderNo);
        } else {
            responseVo.setInvokeStatus(TradePayRefundStatusEnum.REFUND_STATUS_FAILED.getCode().toString());
            responseVo.setErrCode((String) resultMap.get("errCode"));
            responseVo.setErrCodeDes((String) resultMap.get("errMsg"));
        }

        tradeResult.setRetCode(PayConstant.TRADE_STATUS_SUCCESS);
        tradeResult.setRetData(responseVo);
        _log.info("打印：" + JSON.toJSONString(tradeResult));

        return tradeResult;
    }

    /**
     * 退款
     *
     * @param resultMap
     * @param responseVo
     * @return
     */
    public VoResTradePayment getRefundResultMap(Map resultMap, VoResRefund responseVo) {
        VoResTradePayment tradeResult = new VoResTradePayment();

        responseVo.setErrCode((String) resultMap.get("errCode"));
        responseVo.setErrCodeDes((String) resultMap.get("errMsg"));

        if (!PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase((String) resultMap.getOrDefault("retCode", ""))) {
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            _log.warn("推送到沦陷队列处理");
            VoPollingRefund pollingRefund = new VoPollingRefund();
            pollingRefund.setMchId(responseVo.getMchId());
            pollingRefund.setChannelId(responseVo.getChannelId());
            pollingRefund.setMchOrderNo(responseVo.getMchOrderNo());
            pollingRefund.setMchRefundNo(responseVo.getMchRefundNo());
            this.pollingRefund(pollingRefund);
        } else {
            String channelRefundOrderNo = String.valueOf(resultMap.get("channelRefundOrderNo"));
            responseVo.setChannelRefundNo(channelRefundOrderNo);
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_SUCCESS);
            String mchId = responseVo.getMchId();
            String mchOrderNo = responseVo.getMchOrderNo();
            String channelOrderNo = responseVo.getChannelOrderNo();
            String mchRefundNo = responseVo.getMchRefundNo();
            String errCode = responseVo.getErrCode();
            String errMsg = responseVo.getErrCodeDes();
            completeSuccessRefundOrder(mchId,mchOrderNo,channelOrderNo,mchRefundNo,channelRefundOrderNo,errCode,errMsg);
        }

        tradeResult.setRetData(responseVo);
        _log.info("打印：" + JSON.toJSONString(tradeResult));
        return tradeResult;
    }

    /**
     * 扫码支付结果分析
     *
     * @param resultMap
     * @param responseVo
     * @return
     */
    public VoResTradePayment getWapPayResultMap(Map resultMap, VoResPayWap responseVo) {
        VoResTradePayment tradeResult = new VoResTradePayment();

        responseVo.setErrCode((String) resultMap.get("errCode"));
        responseVo.setErrCodeDes((String) resultMap.get("errMsg"));

        String retCode = (String) resultMap.getOrDefault("retCode", "");
        if (PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase(retCode)) {
            String mchId = responseVo.getMchId();
            String mchOrderNo = responseVo.getMchOrderNo();
            String body = String.valueOf(resultMap.get("body"));
            responseVo.setCodeUrl(body);
            responseVo.setPrepayId(String.valueOf(resultMap.get("prepayId")));
            responseVo.setMchId(mchId);
            responseVo.setMchOrderNo(mchOrderNo);

            //completePayingOrder(mchId, mchOrderNo, channelOrderNo, null);
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_SUCCESS);
            _log.warn("推送到沦陷队列处理");
            VoPollingPay pollingPay = new VoPollingPay();
            pollingPay.setMchId(mchId);
            pollingPay.setChannelId(responseVo.getChannelId());
            pollingPay.setMchOrderNo(mchOrderNo);
            this.pollingPay(pollingPay);
        } else {
            tradeResult.setRetCode(PayConstant.TRADE_STATUS_FAILED);
        }

        tradeResult.setRetData(responseVo);
        _log.info("打印：" + JSON.toJSONString(tradeResult));
        return tradeResult;
    }

    /**
     * 轮询支付类订单
     * @param pollingPay
     */
    public void pollingPay(VoPollingPay pollingPay){
        _log.info("开始查询支付订单" + pollingPay.getMchOrderNo());
        try {
            int cnt = pollingConfig.getCount();
            pollingPay.setProcessCount(cnt);
            mq4PayPolling.send(pollingPay,10000);
            _log.info("推送到收费队列成功(10秒后开始轮询)");
        } catch (Exception e) {
            e.printStackTrace();
            _log.error("异常：" + e.getMessage());
        }
    }

    /**
     * 轮询退款类订单
     * @param pollingRefund
     */
    public void pollingRefund(VoPollingRefund pollingRefund){
        _log.info("开始查询退费订单" + pollingRefund.getMchRefundNo());
        try {
            int cnt = pollingConfig.getCount();
            pollingRefund.setProcessCount(cnt);
            mq4RefundPolling.send(pollingRefund,10000);
            _log.info("推送到退费队列成功(10秒后开始轮询)");
        } catch (Exception e) {
            e.printStackTrace();
            _log.error("异常：" + e.getMessage());
        }
    }

    /**
     * 更新扫码支付 二维码
     *
     * @param mchId
     * @param mchOrderNo
     * @param codeUrl
     */
    private void completePayingOrder(String mchId, String mchOrderNo, String channelOrderNo, String codeUrl) {
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(mchId, mchOrderNo);
        LambdaUpdateWrapper<TradePayOrder> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(TradePayOrder::getErrCode,"");//tradePayOrder.setErrCode("");
        if (StringUtils.isNotBlank(codeUrl)) {
            updateWrapper.set(TradePayOrder::getAuthCode,codeUrl);//tradePayOrder.setAuthCode(codeUrl);
            _log.info("扫码支付更新二维码：" + codeUrl);
        }
        if (StringUtils.isNotBlank(channelOrderNo)) {
            updateWrapper.set(TradePayOrder::getChannelOrderNo,channelOrderNo);//tradePayOrder.setChannelOrderNo(channelOrderNo);
            _log.info("查询更新渠道订单号：" + channelOrderNo);
        }
        updateWrapper.set(TradePayOrder::getStatus,TradePayStatusEnum.PAY_STATUS_PAYING.getCode());//tradePayOrder.setStatus(PayConstant.PAY_STATUS_PAYING);
        updateWrapper.set(TradePayOrder::getInvokeStatus,TradePayStatusEnum.PAY_STATUS_PAYING.name());//tradePayOrder.setInvokeStatus(TradePayStatusEnum.PAY_STATUS_PAYING.name());
        updateWrapper.eq(TradePayOrder::getId,tradePayOrder.getId());
        updateWrapper.eq(TradePayOrder::getMchId,tradePayOrder.getMchId());
        updateWrapper.eq(TradePayOrder::getMchOrderNo,tradePayOrder.getMchOrderNo());
        tradePayOrderService.updateTradePayOrder(updateWrapper);//tradePayOrderService.updateTradePayOrder(tradePayOrder);
        _log.info("订单{}更新二维码成功.", mchOrderNo);
    }

    /**
     * 更新失败订单 超时关闭、支付失败等
     * @param mchId
     * @param mchOrderNo
     * @param errCode
     * @param errCodeDes
     * @param invokeStatus
     */
    private void completeFailedOrder(String mchId, String mchOrderNo,String errCode,String errCodeDes,String invokeStatus){
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(mchId, mchOrderNo);
        if (PayConstant.PAY_STATUS_SUCCESS.equals(tradePayOrder.getStatus())){
            _log.warn("支付成功的暂时不处理");
            return;
        } else if (PayConstant.PAY_STATUS_COMPLETE.equals(tradePayOrder.getStatus())){
            _log.warn("支付完成(成功)的暂时不处理");
            return;
        }
        LambdaUpdateWrapper<TradePayOrder> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(TradePayOrder::getErrCode,errCode);
        updateWrapper.set(TradePayOrder::getErrMsg,errCodeDes);
        updateWrapper.set(TradePayOrder::getInvokeStatus,invokeStatus);
        updateWrapper.eq(TradePayOrder::getMchId,mchId);
        updateWrapper.eq(TradePayOrder::getMchOrderNo,mchOrderNo);
        tradePayOrderService.updateTradePayOrder(updateWrapper);
        _log.warn("失败订单{}更新成功", mchOrderNo);
    }

    /**
     * 支付成功方法
     *
     * @param mchId
     * @param mchOrderNo
     * @param channelOrderNo
     */
    private void completeSuccessOrder(String mchId, String mchOrderNo, String channelOrderNo, String errCode, String errMsg) {
        TradePayOrder tradePayOrder = tradePayOrderService.getTradePayOrder(mchId, mchOrderNo);
        LambdaUpdateWrapper<TradePayOrder> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(TradePayOrder::getStatus,TradePayStatusEnum.PAY_STATUS_SUCCESS.getCode());//tradePayOrder.setStatus(PayConstant.PAY_STATUS_SUCCESS);
        if (StringUtils.isNotBlank(channelOrderNo)) {
            updateWrapper.set(TradePayOrder::getChannelOrderNo,channelOrderNo);//tradePayOrder.setChannelOrderNo(channelOrderNo);
        }
        if (StringUtils.isNotBlank(errCode)){
            updateWrapper.set(TradePayOrder::getErrCode,errCode);
        }
        if (StringUtils.isNotBlank(errMsg)){
            updateWrapper.set(TradePayOrder::getErrMsg,errMsg);
        }
        updateWrapper.set(TradePayOrder::getInvokeStatus,TradePayStatusEnum.PAY_STATUS_SUCCESS.name());//tradePayOrder.setInvokeStatus(TradePayStatusEnum.PAY_STATUS_SUCCESS.name());
        updateWrapper.set(TradePayOrder::getPaySuccTime,new Date());//tradePayOrder.setPaySuccTime(System.currentTimeMillis());
        updateWrapper.eq(TradePayOrder::getId,tradePayOrder.getId());
        updateWrapper.eq(TradePayOrder::getMchId,tradePayOrder.getMchId());
        updateWrapper.eq(TradePayOrder::getMchOrderNo,tradePayOrder.getMchOrderNo());
        tradePayOrderService.updateTradePayOrder(updateWrapper);//tradePayOrderService.updateTradePayOrder(tradePayOrder);
        _log.info("订单{}支付成功.", mchOrderNo);
    }

    /**
     * 退款成功
     * @param mchId
     * @param mchOrderNo
     * @param channelOrderNo
     * @param mchRefundNo
     * @param channelRefundNo
     */
    private void completeSuccessRefundOrder(String mchId, String mchOrderNo, String channelOrderNo, String mchRefundNo, String channelRefundNo, String errCode, String errMsg) {
        TradeRefundOrder refundOrder = tradeRefundOrderService.getTradeRefundOrder(mchId,mchOrderNo,mchRefundNo);
        LambdaUpdateWrapper<TradeRefundOrder> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(TradeRefundOrder::getStatus,TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.getCode());//refundOrder.setStatus(TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.getCode());
        updateWrapper.set(TradeRefundOrder::getResult,TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.getDesc());//refundOrder.setResult(TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.getDesc());
        if (StringUtils.isBlank(refundOrder.getChannelOrderNo())) {
            updateWrapper.set(TradeRefundOrder::getChannelOrderNo,channelRefundNo);//refundOrder.setChannelOrderNo(channelOrderNo);
        }
        updateWrapper.set(TradeRefundOrder::getRefundSuccTime,new Date());
        if (StringUtils.isNotBlank(errCode)){
            updateWrapper.set(TradeRefundOrder::getChannelErrCode,errCode);
        }
        if (StringUtils.isNotBlank(errMsg)){
            updateWrapper.set(TradeRefundOrder::getChannelErrMsg,errMsg);
        }
        updateWrapper.eq(TradeRefundOrder::getId,refundOrder.getId());
        updateWrapper.eq(TradeRefundOrder::getMchId,refundOrder.getMchId());
        updateWrapper.eq(TradeRefundOrder::getMchRefundNo,refundOrder.getMchRefundNo());
        tradeRefundOrderService.updateTradeRefundOrder(updateWrapper);
    }

    /**
     * 订单撤销成功
     * @param mchId
     * @param mchOrderNo
     * @param channelRefundNo
     */
    private void completeSuccessRefundOrder(String mchId, String mchOrderNo,String channelRefundNo, String errCode, String errMsg) {
        TradeRefundOrder refundOrder = tradeRefundOrderService.getTradeRefundOrder(mchId,mchOrderNo,null);
        LambdaUpdateWrapper<TradeRefundOrder> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(TradeRefundOrder::getStatus,TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.getCode());
        updateWrapper.set(TradeRefundOrder::getResult,TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.getDesc());
        if (StringUtils.isBlank(refundOrder.getChannelOrderNo()) && StringUtils.isNotBlank(channelRefundNo)) {
            updateWrapper.set(TradeRefundOrder::getChannelOrderNo,channelRefundNo);//refundOrder.setChannelOrderNo(channelOrderNo);
        }
        updateWrapper.set(TradeRefundOrder::getRefundSuccTime,new Date());
        if (StringUtils.isNotBlank(errCode)){
            updateWrapper.set(TradeRefundOrder::getChannelErrCode,errCode);
        }
        if (StringUtils.isNotBlank(errMsg)){
            updateWrapper.set(TradeRefundOrder::getChannelErrMsg,errMsg);
        }
        updateWrapper.eq(TradeRefundOrder::getId,refundOrder.getId());
        updateWrapper.eq(TradeRefundOrder::getMchId,refundOrder.getMchId());
        updateWrapper.eq(TradeRefundOrder::getMchRefundNo,refundOrder.getMchRefundNo());
        tradeRefundOrderService.updateTradeRefundOrder(updateWrapper);
    }
}
