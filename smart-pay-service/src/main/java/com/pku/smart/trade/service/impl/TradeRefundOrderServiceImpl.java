package com.pku.smart.trade.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.trade.entity.TradeRefundOrder;
import com.pku.smart.trade.mapper.TradeRefundOrderMapper;
import com.pku.smart.trade.service.ITradeRefundOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TradeRefundOrderServiceImpl implements ITradeRefundOrderService {

    @Autowired
    TradeRefundOrderMapper tradeRefundOrderMapper;

    /**
     * 查询商户下退款单号
     *
     * @param mchId
     * @param mchRefundNo
     * @return
     */
    @Override
    public TradeRefundOrder getTradeRefundOrder(String mchId, String mchRefundNo) {
        LambdaQueryWrapper<TradeRefundOrder> queryWrapper = new LambdaQueryWrapper<>();//QueryWrapper<TradeRefundOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(TradeRefundOrder::getMchId,mchId);//queryWrapper.eq("mch_id",mchId);
        queryWrapper.eq(TradeRefundOrder::getMchRefundNo,mchRefundNo);//queryWrapper.eq("mch_refund_no",mchRefundNo);
        return tradeRefundOrderMapper.selectOne(queryWrapper);
    }

    /***
     * 根据商户订单号和退款订单号查询
     * @param mchId
     * @param mchOrderNo
     * @param mchRefundNo
     * @return
     */
    @Override
    public TradeRefundOrder getTradeRefundOrder(String mchId, String mchOrderNo, String mchRefundNo) {
        LambdaQueryWrapper<TradeRefundOrder> queryWrapper = new LambdaQueryWrapper<>();//QueryWrapper<TradeRefundOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(TradeRefundOrder::getMchId,mchId);//queryWrapper.eq("mch_id",mchId);
        queryWrapper.eq(TradeRefundOrder::getMchOrderdNo,mchOrderNo);//queryWrapper.eq("mch_order_no",mchOrderNo);
        if (StringUtils.isNotBlank(mchRefundNo)) {
            queryWrapper.eq(TradeRefundOrder::getMchRefundNo, mchRefundNo);//queryWrapper.eq("mch_refund_no",mchRefundNo);
        }
        return tradeRefundOrderMapper.selectOne(queryWrapper);
    }

    /**
     * 更新退款订单
     *
     * @param refundOrder
     * @return
     */
    @Override
    public int insertTradeRefundOrder(TradeRefundOrder refundOrder) {
        refundOrder.setCreateTime(new Date());
        refundOrder.setCreateBy(PayConstant.TRADE_DEFAULT_OPERA);
        refundOrder.setUpdateTime(new Date());
        refundOrder.setUpdateBy(PayConstant.TRADE_DEFAULT_OPERA);
        return tradeRefundOrderMapper.insert(refundOrder);
    }

    /**
     * 更新退款订单
     *
     * @param refundOrder
     * @return
     */
    @Override
    public int updateTradeRefundOrder(TradeRefundOrder refundOrder) {
        refundOrder.setUpdateTime(new Date());
        refundOrder.setUpdateBy(PayConstant.TRADE_DEFAULT_OPERA);
        return tradeRefundOrderMapper.updateById(refundOrder);
    }

    @Override
    public int updateTradeRefundOrder(LambdaUpdateWrapper<TradeRefundOrder> updateWrapper) {
        updateWrapper.set(TradeRefundOrder::getUpdateTime,new Date());
        updateWrapper.set(TradeRefundOrder::getUpdateBy,PayConstant.TRADE_DEFAULT_OPERA);
        return tradeRefundOrderMapper.update(null,updateWrapper);
    }
}
