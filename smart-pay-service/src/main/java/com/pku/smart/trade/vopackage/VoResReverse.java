package com.pku.smart.trade.vopackage;

import com.pku.smart.base.VoResBase;
import lombok.Data;

import java.io.Serializable;

@Data
public class VoResReverse extends VoResBase implements Serializable {
    /**
     * 商户编码
     */
    private String mchId;

    /**
     * 渠道编码
     */
    private String channelId;

    /**
     * 商户订单号
     */
    private String mchOrderNo;

    /**
     * 渠道订单号
     */
    private String channelOrderNo;

}
