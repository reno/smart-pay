package com.pku.smart.trade.vopackage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value = "VoReqRefund",description = "退款入参")
public class VoReqRefund implements Serializable {
    /**
     * 商户编码
     */
    @ApiModelProperty(value = "商户编码", required = true, example = "10000000")
    @NotNull(message = "商户编码[mchId]不能为空")
    private String mchId;

    /**
     * 渠道编码
     */
    @ApiModelProperty(value = "渠道编码")
    private String channelId;

    /**
     * 商户订单号
     */
    @ApiModelProperty(value = "商品订单号", required = true)
    @NotNull(message = "商户订单号[mchOrderNo]不能为空")
    private String mchOrderNo;

    /**
     * 渠道订单号
     */
    @ApiModelProperty(value = "渠道订单号")
    private String channelOrderNo;

    /**
     * 商户退款单号
     */
    @ApiModelProperty(value = "商户退款单号", required = true)
    @NotNull(message = "商户退款单号[mchRefundNo]不能为空")
    private String mchRefundNo;

    /**
     * 退款原因
     */
    @ApiModelProperty(value = "退款原因")
    private String refundReason;

    /**
     * 退款金额 (撤销默认是支付金额)
     */
    @ApiModelProperty(value = "退款金额", required = true)
    @NotNull(message = "退款金额[refundAmount]不能为空")
    private Long refundAmount;

    /**
     * 订单金额
     */
    @ApiModelProperty(value = "订单金额")
    private Long totalAmount;

    /**
     * 商户回调地址
     */
    @ApiModelProperty(value = "商户回调地址", required = true)
    @NotNull(message = "商户回调地址[notifyUrl]不能为空")
    private String notifyUrl;

    /**
     * 签名
     */
    @ApiModelProperty(value = "签名", required = true)
    @NotNull(message = "签名[sign]不能为空")
    private String sign;
}
