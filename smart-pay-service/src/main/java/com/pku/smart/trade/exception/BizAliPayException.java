package com.pku.smart.trade.exception;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayResponse;
import com.google.common.base.Joiner;
import lombok.Data;

@Deprecated
@Data
public class BizAliPayException extends AlipayApiException {
    private String code;
    private String msg;
    private String sub_code;
    private String sub_msg;
    private String jsonString;

    private BizAliPayException(BizAliPayException.Builder builder) {
        super(builder.buildErrorMsg());
        this.code = builder.code;
        this.msg = builder.msg;
        this.sub_code = builder.sub_code;
        this.sub_msg = builder.sub_msg;
        this.jsonString = builder.jsonString;
    }

    public static BizAliPayException.Builder newBuilder() {
        return new BizAliPayException.Builder();
    }

    public static BizAliPayException from(AlipayResponse response) {
        return newBuilder().jsonString(response.getBody()).msg(response.getMsg()).code(response.getCode()).sub_code(response.getSubCode()).sub_msg(response.getSubMsg()).build();
    }

    public static final class Builder {
        private String code;
        private String msg;
        private String sub_code;
        private String sub_msg;
        private String jsonString;

        private Builder() {
        }

        public BizAliPayException.Builder code(String code) {
            this.code = code;
            return this;
        }

        public BizAliPayException.Builder msg(String msg) {
            this.msg = msg;
            return this;
        }

        public BizAliPayException.Builder sub_code(String sub_code) {
            this.sub_code = sub_code;
            return this;
        }

        public BizAliPayException.Builder sub_msg(String sub_msg) {
            this.sub_msg = sub_msg;
            return this;
        }

        public BizAliPayException.Builder jsonString(String jsonString) {
            this.jsonString = jsonString;
            return this;
        }

        public BizAliPayException build() {
            return new BizAliPayException(this);
        }

        public String buildErrorMsg() {
            return Joiner.on("，").skipNulls().join(this.code == null ? null : String.format("返回代码：[%s]", this.code), this.msg == null ? null : String.format("返回信息：[%s]", this.msg), new Object[]{this.code == null ? null : String.format("结果代码：[%s]", this.code), this.sub_code == null ? null : String.format("错误代码：[%s]", this.sub_code), this.sub_msg == null ? null : String.format("错误详情：[%s]", this.sub_msg), this.jsonString == null ? null : "支付宝返回的原始报文：\n" + this.jsonString});
        }
    }
}
