package com.pku.smart.account.service;

import com.pku.smart.account.entity.AccountPayChannel;

import java.util.List;

public interface IAccountPayChannelService {

    /**
     * 根据渠道编码查询支付渠道
     * @param channelId
     * @return
     */
    AccountPayChannel getPayChannel(String mchId, String channelId);

    /**
     * 根据渠道名称查询支付渠道列表
     * @param mchId
     * @param channelName
     * @return
     */
    List<AccountPayChannel> getPayChannelList(String mchId, String channelName);

    /**
     * 根据商户编号查询支付渠道
     * @param mchId
     * @return
     */
    List<AccountPayChannel> getPayChannelList(String mchId);
}
