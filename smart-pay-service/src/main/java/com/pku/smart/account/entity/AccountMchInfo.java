package com.pku.smart.account.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pku.smart.base.BaseEntity;
import lombok.Data;

@Data
@TableName(value = "pm_mch_info")
public class AccountMchInfo extends BaseEntity {

    /**主键*/
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 商户ID
     *
     * @mbggenerated
     */
    @TableField(value = "mch_id")
    private String mchId;

    /**
     * 名称
     *
     * @mbggenerated
     */
    @TableField(value = "name")
    private String name;

    /**
     * 类型
     *
     * @mbggenerated
     */
    @TableField(value = "type")
    private String type;

    /**
     * 请求私钥
     *
     * @mbggenerated
     */
    @TableField(value = "req_key")
    private String reqKey;

    /**
     * 响应私钥
     *
     * @mbggenerated
     */
    @TableField(value = "res_key")
    private String resKey;

    /**
     * 商户状态,0-停止使用,1-使用中
     *
     * @mbggenerated
     */
    @TableField(value = "status")
    private String status;
}
