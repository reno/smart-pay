package com.pku.smart.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitmqConfig {

    public static final String POLLING_PAY_KEY = "polling.pay.key";
    public static final String POLLING_PAY_QUEUE = "polling.pay.queue";
    public static final String POLLING_PAY_EXCHANGE = "polling.pay.exchange";

    public static final String POLLING_REFUND_KEY = "polling.refund.key";
    public static final String POLLING_REFUND_QUEUE = "polling.refund.queue";
    public static final String POLLING_REFUND_EXCHANGE = "polling.refund.exchange";

    /**
     * 延时队列交换机
     * 注意这里的交换机类型：CustomExchange
     *
     * @return
     */
    @Bean
    public CustomExchange payExchange() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        //属性参数 交换机名称 交换机类型 是否持久化 是否自动删除 配置参数
        return new CustomExchange(POLLING_PAY_EXCHANGE, "x-delayed-message", true, false, args);
    }

    @Bean
    public CustomExchange refundExchange() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        //属性参数 交换机名称 交换机类型 是否持久化 是否自动删除 配置参数
        return new CustomExchange(POLLING_REFUND_EXCHANGE, "x-delayed-message", true, false, args);
    }

    /**
     * 延时队列
     *
     * @return
     */
    @Bean
    public Queue payQueue() {
        //属性参数 队列名称 是否持久化
        return new Queue(POLLING_PAY_QUEUE, true);
    }

    @Bean
    public Queue refundQueue() {
        //属性参数 队列名称 是否持久化
        return new Queue(POLLING_REFUND_QUEUE, true);
    }

    /**
     * 给延时队列绑定交换机
     *
     * @return
     */
    @Bean
    public Binding cfgPayBinding() {
        return BindingBuilder.bind(payQueue()).to(payExchange()).with(POLLING_PAY_KEY).noargs();
    }

    @Bean
    public Binding cfgRefundBinding() {
        return BindingBuilder.bind(refundQueue()).to(refundExchange()).with(POLLING_REFUND_KEY).noargs();
    }
}
