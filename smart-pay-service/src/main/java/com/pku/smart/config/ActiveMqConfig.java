//package com.pku.smart.config;
//
//import org.apache.activemq.command.ActiveMQQueue;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.jms.Queue;
//
//@Configuration
//public class ActiveMqConfig {
//
//    /**
//     * 订单商户通知队列
//     */
//    public static final String PAY_NOTIFY_QUEUE_NAME = "pay.notify.queue";
//
//    /**
//     * 订单状态沦陷队列
//     */
//    public static final String PAY_POLLING_QUEUE_NAME = "pay.polling.queue";
//
//    /**
//     * 订单状态沦陷队列
//     */
//    public static final String REFUND_POLLING_QUEUE_NAME = "refund.polling.queue";
//
//    @Bean
//    public Queue payNotifyQueue() {
//        return new ActiveMQQueue(PAY_NOTIFY_QUEUE_NAME);
//    }
//
//    @Bean
//    public Queue payPollingQueue() {
//        return new ActiveMQQueue(PAY_POLLING_QUEUE_NAME);
//    }
//
//    @Bean
//    public Queue refundPollingQueue() {
//        return new ActiveMQQueue(REFUND_POLLING_QUEUE_NAME);
//    }
//}
