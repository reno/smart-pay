package com.pku.smart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix="config.wx")
public class WechatPayConfig {

    private String certRootPath;

    private String notifyUrl;

    private String signType;

    private Boolean isSandbox = true;// 是否沙箱环境,1:沙箱,0:正式环境

    private String billPath;

//    /**
//     * 获取微信支付配置
//     * @param configParam
//     * @param tradeType
//     * @param certRootPath
//     * @param notifyUrl
//     * @return
//     */
//    public static WxPayConfig getWxPayConfig(String configParam, String tradeType, String certRootPath, String notifyUrl) {
//        WxPayConfig wxPayConfig = new WxPayConfig();
//        JSONObject paramObj = JSON.parseObject(configParam);
//        wxPayConfig.setMchId(paramObj.getString("mchId"));
//        wxPayConfig.setAppId(paramObj.getString("appId"));
//        wxPayConfig.setKeyPath(certRootPath + File.separator + paramObj.getString("certLocalPath"));
//        wxPayConfig.setMchKey(paramObj.getString("key"));
//        wxPayConfig.setNotifyUrl(notifyUrl);
//        wxPayConfig.setTradeType(tradeType);
//        wxPayConfig.setUseSandboxEnv(isSandbox);
//        return wxPayConfig;
//    }

}
