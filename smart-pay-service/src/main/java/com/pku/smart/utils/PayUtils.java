package com.pku.smart.utils;

import com.pku.smart.httpclient.HttpClient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class PayUtils {

    /**
     * 验证支付中心签名
     *
     * @param params
     * @param key
     * @return
     */
    public static boolean verifyPaySign(Map<String, Object> params, String key) {
        String sign = (String) params.get("sign"); // 签名
        params.remove("sign");    // 不参与签名
        String checkSign = PayDigestUtil.getSign(params, key);
        if (!checkSign.equalsIgnoreCase(sign)) {
            return false;
        }
        return true;
    }

    /**
     * 发起HTTP/HTTPS请求(method=POST)
     *
     * @param url
     * @return
     */
    public static String call4Post(String url) {
        try {
            URL url1 = new URL(url);
            if ("https".equals(url1.getProtocol())) {
                return HttpClient.callHttpsPost(url);
            } else if ("http".equals(url1.getProtocol())) {
                return HttpClient.callHttpPost(url);
            } else {
                return "";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }
}
