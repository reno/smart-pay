package com.pku.smart.notity.service;

import java.util.Map;

public interface IPayMchNotifyService {

    /**
     * 支付订单通知
     *
     * @param channelOrderNo
     */
    void payOrderSend(String channelOrderNo);

    /**
     * 处理微信回调
     * @param mchId
     * @param xmlResult
     * @return
     */
    String handleWxPayNotify(String mchId, String xmlResult);

    /**
     * 处理微信退款回调
     * @param mchId
     * @param xmlResult
     * @return
     */
    String handleWxPayRefundNotify(String mchId, String xmlResult);

    /**
     * 处理支付宝回调
     * @param mchId
     * @param params
     * @return
     */
    String handleAliPayNotify(String mchId, Map params);
}
