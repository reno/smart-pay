package com.pku.smart.notity.controller;

import com.pku.smart.log.MyLog;
import com.pku.smart.notity.service.IPayMchNotifyService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping(value = "/notity")
public class WechatPayNotifyController {
    private static final MyLog _log = MyLog.getLog(WechatPayNotifyController.class);

    @Autowired
    IPayMchNotifyService mchNotifyService;

    @RequestMapping("/wxpay/{mchId}/pay.action")
    public String notityPay(@PathVariable("mchId") String mchId, HttpServletRequest request) throws IOException {
        _log.info("====== 开始接收微信支付回调通知 ======");
        _log.info("打印商户号：{}", mchId);
        String xml = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
        _log.info("支付通知请求数据:{}", xml);
        String notifyRes = mchNotifyService.handleWxPayNotify(mchId, xml);
        _log.info(notifyRes);
        return notifyRes;
    }

    @RequestMapping("/wxpay/{mchId}/refund.action")
    public String notityRefund(@PathVariable("mchId") String mchId, HttpServletRequest request) throws IOException {
        _log.info("====== 开始接收微信退款回调通知 ======");
        _log.info("打印商户号：{}", mchId);
        String xml = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
        _log.info("退款通知请求数据:{}", xml);
        String notifyRes = mchNotifyService.handleWxPayRefundNotify(mchId, xml);
        _log.info(notifyRes);
        return mchId;
    }

}
