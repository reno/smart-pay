package com.pku.smart.queue.polling;

import com.alibaba.fastjson.JSON;
import com.pku.smart.config.RabbitmqConfig;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.log.MyLog;
import com.pku.smart.queue.config.PollingConfig;
import com.pku.smart.queue.vopackage.VoPollingPay;
import com.pku.smart.trade.entity.TradePayOrder;
import com.pku.smart.trade.enums.TradePayStatusEnum;
import com.pku.smart.trade.service.IPaymentManagerService;
import com.pku.smart.trade.service.ITradePayOrderService;
import com.pku.smart.trade.vopackage.VoReqQueryPay;
import com.pku.smart.trade.vopackage.VoResQueryPay;
import com.pku.smart.trade.vopackage.VoResTradePayment;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Mq4PayPolling extends Mq4BasePolling {

    private static final MyLog _log = MyLog.getLog(Mq4PayPolling.class);

    @Autowired
    PollingConfig pollingConfig;

    @Autowired
    ITradePayOrderService payOrderService;

    @Autowired
    IPaymentManagerService paymentManagerService;

    public void send(final Object msg, final Integer delay) {
        super.send(msg,delay, RabbitmqConfig.POLLING_PAY_EXCHANGE,RabbitmqConfig.POLLING_PAY_KEY);
    }

    //监听消息队列
    @RabbitListener(queues = RabbitmqConfig.POLLING_PAY_QUEUE)
    public void consumeMessage(VoPollingPay pollingPay) {
        _log.info("do notify task, msg={}", JSON.toJSONString(pollingPay));
        //1、解析消息内容
        pollingPay.setProcessCount(pollingPay.getProcessCount()-1);
        String mchId = pollingPay.getMchId();
        String channelId = pollingPay.getChannelId();
        String mchOrderNo = pollingPay.getMchOrderNo();
        _log.info("商户号={},渠道编码={},商户订单号={}",mchId,channelId,mchOrderNo);
        int count = pollingPay.getProcessCount();
        _log.info("轮询剩余次数：" + count);
        if (count <= 0){
            _log.error("轮询次数已用尽,跳出!");
            return;
        }

        //2、判断订单是否完成，未完成继续下一步
        TradePayOrder payOrder = payOrderService.getTradePayOrder(mchId,mchOrderNo);
        if (payOrder == null){
            _log.info("本地支付订单号{}不存在,跳出轮询.",mchOrderNo);
            return;
        }
        if (PayConstant.PAY_STATUS_SUCCESS.equals(payOrder.getStatus())){
            _log.info("订单{}已经是支付成功状态",mchOrderNo);
            return;
        }
        if (PayConstant.PAY_STATUS_COMPLETE.equals(payOrder.getStatus())){
            _log.info("订单{}已经是支付成功状态(通知成功)",mchOrderNo);
            return;
        }

        //3、查询支付宝微信订单状态//VoResTradePayment payQuery(VoReqQueryPay requestVo);
        VoReqQueryPay requestVo = new VoReqQueryPay();
        requestVo.setMchId(mchId);
        requestVo.setMchOrderNo(mchOrderNo);
        requestVo.setSign("1");
        try {
            VoResTradePayment tradePayment = paymentManagerService.payQuery(requestVo);
            if (!PayConstant.TRADE_STATUS_SUCCESS.equals(tradePayment.getRetCode())){
                int cnt = pollingConfig.getCount()-count;
                _log.error("支付订单{}查询未成功，延迟{}秒后继续，继续轮询",mchOrderNo,cnt * 15);
                this.send(pollingPay, cnt * 15 * 1000);
                return;
            }
            VoResQueryPay responseVo = (VoResQueryPay)tradePayment.getRetData();
            String invokeStatus = responseVo.getInvokeStatus();
            if (StringUtils.isNotBlank(invokeStatus) && TradePayStatusEnum.PAY_STATUS_SUCCESS.name().equals(invokeStatus)){
                _log.info("订单{}支付成功，跳出轮询okk",mchOrderNo);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            int cnt = pollingConfig.getCount()-count;
            _log.error("订单订单{}查询异常，延迟{}秒后继续，继续轮询.本次异常：{}",mchOrderNo,cnt * 15,e.getMessage());
            this.send(pollingPay, cnt * 15 * 1000);
            return;
        }
        int cnt = pollingConfig.getCount()-count;
        _log.error("支付订单{}未支付成功，延迟{}秒后继续，继续轮询",mchOrderNo,cnt * 15);
        this.send(pollingPay, cnt * 15 * 1000);
    }

//    /**
//     * 发送延迟消息
//     * @param msg
//     * @param delay
//     */
//    public void send(final String msg, final long delay) {
//        super.send(msg,delay,super.payPollingQueue);
//    }
//
//    /**
//     * 接收队列消息 处理订单状态
//     * @param msg
//     */
//    @JmsListener(destination = ActiveMqConfig.PAY_POLLING_QUEUE_NAME)
//    public void receive(String msg) {
//        _log.info("do notify task, msg={}", msg);
//
//        //1、解析消息内容
//        VoPollingPay pollingPay = JSON.parseObject(msg,VoPollingPay.class);
//        pollingPay.setProcessCount(pollingPay.getProcessCount()-1);
//        String mchId = pollingPay.getMchId();
//        String channelId = pollingPay.getChannelId();
//        String mchOrderNo = pollingPay.getMchOrderNo();
//        _log.info("商户号={},渠道编码={},商户订单号={}",mchId,channelId,mchOrderNo);
//        int count = pollingPay.getProcessCount();
//        _log.info("轮询剩余次数：" + count);
//        if (count <= 0){
//            _log.error("轮询次数已用尽,跳出!");
//            return;
//        }
//
//        //2、判断订单是否完成，未完成继续下一步
//        TradePayOrder payOrder = payOrderService.getTradePayOrder(mchId,mchOrderNo);
//        if (payOrder == null){
//            _log.info("本地支付订单号{}不存在,跳出轮询.",mchOrderNo);
//            return;
//        }
//        if (PayConstant.PAY_STATUS_SUCCESS.equals(payOrder.getStatus())){
//            _log.info("订单{}已经是支付成功状态",mchOrderNo);
//            return;
//        }
//        if (PayConstant.PAY_STATUS_COMPLETE.equals(payOrder.getStatus())){
//            _log.info("订单{}已经是支付成功状态(通知成功)",mchOrderNo);
//            return;
//        }
//
//        //3、查询支付宝微信订单状态//VoResTradePayment payQuery(VoReqQueryPay requestVo);
//        VoReqQueryPay requestVo = new VoReqQueryPay();
//        requestVo.setMchId(mchId);
//        requestVo.setMchOrderNo(mchOrderNo);
//        requestVo.setSign("1");
//        VoResTradePayment tradePayment = paymentManagerService.payQuery(requestVo);
//        if (!PayConstant.TRADE_STATUS_SUCCESS.equals(tradePayment.getRetCode())){
//            int cnt = 5-count;
//            _log.error("支付订单{}查询未成功，延迟{}秒后继续，继续轮询",mchOrderNo,cnt * 15);
//            this.send(JSON.toJSONString(pollingPay), cnt * 15 * 1000L);
//            return;
//        }
//        VoResQueryPay responseVo = (VoResQueryPay)tradePayment.getRetData();
//        String invokeStatus = responseVo.getInvokeStatus();
//        if (StringUtils.isNotBlank(invokeStatus) && TradePayStatusEnum.PAY_STATUS_SUCCESS.getCode().toString().equals(invokeStatus)){
//            _log.info("订单{}支付成功，跳出轮询okk",mchOrderNo);
//            return;
//        }
//        int cnt = 5-count;
//        _log.error("支付订单{}未支付成功，延迟{}秒后继续，继续轮询",mchOrderNo,cnt * 15);
//        this.send(JSON.toJSONString(pollingPay), cnt * 15 * 1000);
//    }
}
