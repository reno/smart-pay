package com.pku.smart.fiance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencsv.bean.CsvBindByPosition;
import com.pku.smart.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付宝对账单
 */
@Data
@TableName(value = "pm_fiance_al")
public class FianceAl extends BaseEntity implements Serializable {

    @CsvBindByPosition(position=0)
    @TableField(value = "alorder")
    private String alorder;//支付宝交易号

    @CsvBindByPosition(position=1)
    @TableField(value = "bzorder")
    private String bzorder;//商户订单号

    @CsvBindByPosition(position=2)
    @TableField(value = "tradetype")
    private String tradetype;//业务类型

    @CsvBindByPosition(position=3)
    @TableField(value = "bztitle")
    private String bztitle;//商品名称

    @CsvBindByPosition(position=4)
    @TableField(value = "createtime")
    private String createtime;//创建时间

    @CsvBindByPosition(position=5)
    @TableField(value = "tradetime")
    private String tradetime;//完成时间

    @CsvBindByPosition(position=6)
    @TableField(value = "much")
    private String much;//门店编号

    @CsvBindByPosition(position=8)
    @TableField(value = "opera")
    private String opera;//操作员

    @CsvBindByPosition(position=9)
    @TableField(value = "deviceid")
    private String deviceid;//终端号

    @CsvBindByPosition(position=10)
    @TableField(value = "tradeuser")
    private String tradeuser;//对方账户

    @CsvBindByPosition(position=11)
    @TableField(value = "totalmoney")
    private BigDecimal totalmoney;//订单金额（元）

    @CsvBindByPosition(position=12)
    @TableField(value = "trademoney")
    private BigDecimal trademoney;//商家实收（元）

    @CsvBindByPosition(position=13)
    @TableField(value = "redpacketmoney")
    private BigDecimal redpacketmoney;//支付宝红包（元）

    @CsvBindByPosition(position=14)
    @TableField(value = "jifenmoney")
    private BigDecimal jifenmoney;//集分宝（元）

    @CsvBindByPosition(position=15)
    @TableField(value = "alipaytax")
    private BigDecimal alipaytax;//支付宝优惠（元）

    @CsvBindByPosition(position=16)
    @TableField(value = "tradetax")
    private BigDecimal tradetax;//商家优惠（元）

    @CsvBindByPosition(position=17)
    @TableField(value = "cancletax")
    private BigDecimal cancletax;//券核销金额（元）

    @CsvBindByPosition(position=18)
    @TableField(value = "taxname")
    private String taxname;//券名称

    @CsvBindByPosition(position=19)
    @TableField(value = "redpacketmoneytax")
    private BigDecimal redpacketmoneytax;//商家红包消费金额（元）

    @CsvBindByPosition(position=20)
    @TableField(value = "cardtrademoney")
    private BigDecimal cardtrademoney;//卡消费金额（元）

    @CsvBindByPosition(position=21)
    @TableField(value = "tforder")
    private String tforder;//退款批次号/请求号

    @CsvBindByPosition(position=22)
    @TableField(value = "servicemoney")
    private BigDecimal servicemoney;//服务费（元）

    @CsvBindByPosition(position=23)
    @TableField(value = "fenrun")
    private BigDecimal fenrun;//分润（元）

    @CsvBindByPosition(position=24)
    @TableField(value = "mark")
    private String mark;//备注

    @TableField(value = "bill_date")
    private Date date;//对账日期

    @TableField(value = "mch_id")
    private String mchId;

    /**
     * 渠道名称,如:alipay,wechat
     *
     * @mbggenerated
     */
    @TableField(value = "channel_name")
    private String channelName;

    /**主键*/
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;
}
