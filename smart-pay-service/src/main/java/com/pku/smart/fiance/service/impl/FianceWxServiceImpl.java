package com.pku.smart.fiance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pku.smart.fiance.entity.FianceWx;
import com.pku.smart.fiance.mapper.FianceWxMapper;
import com.pku.smart.fiance.service.IFianceWxService;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class FianceWxServiceImpl extends ServiceImpl<FianceWxMapper, FianceWx> implements IFianceWxService {

    @Override
    public void deleteWx(String mchId, Date deleteDate) {
        LambdaQueryWrapper<FianceWx> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FianceWx::getMchId,mchId);
        queryWrapper.eq(FianceWx::getDate,deleteDate);
        remove(queryWrapper);
    }
}
