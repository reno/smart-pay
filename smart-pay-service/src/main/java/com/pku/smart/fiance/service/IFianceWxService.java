package com.pku.smart.fiance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pku.smart.fiance.entity.FianceWx;

import java.util.Date;

public interface IFianceWxService extends IService<FianceWx> {

    void deleteWx(String mchId, Date deleteDate);
}
