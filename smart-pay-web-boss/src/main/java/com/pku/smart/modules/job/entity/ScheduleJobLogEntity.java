/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.pku.smart.modules.job.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 定时任务日志
 *
 * @author Mark sunlightcs@gmail.com
 */
@Data
@TableName("schedule_job_log")
public class ScheduleJobLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 日志id
	 */
	@TableId(value = "log_id")
	private Long logId;
	
	/**
	 * 任务id
	 */
	@TableField(value = "job_id")
	private Long jobId;
	
	/**
	 * spring bean名称
	 */
	@TableField(value = "bean_name")
	private String beanName;
	
	/**
	 * 参数
	 */
	@TableField(value = "params")
	private String params;
	
	/**
	 * 任务状态    0：成功    1：失败
	 */
	@TableField(value = "status")
	private Integer status;
	
	/**
	 * 失败信息
	 */
	@TableField(value = "error")
	private String error;
	
	/**
	 * 耗时(单位：毫秒)
	 */
	@TableField(value = "times")
	private Integer times;
	
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(value = "create_time")
	private Date createTime;
	
}
