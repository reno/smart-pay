///**
// * Copyright (c) 2016-2019 人人开源 All rights reserved.
// *
// * https://www.renren.io
// *
// * 版权所有，侵权必究！
// */
//
//package com.pku.smart.common.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//
///**
// * Swagger配置
// *
// * @author Mark sunlightcs@gmail.com
// */
//@Configuration
//public class SwaggerConfig{
//
//    @Bean
//    public Docket createRestApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//            .apiInfo(apiInfo())
//            .select()
//            //加了ApiOperation注解的类，生成接口文档
//            //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
//            //包下的类，生成接口文档
//            //.apis(RequestHandlerSelectors.basePackage("io.renren.modules.job.controller"))
//            .paths(PathSelectors.any())
//            .build();
//    }
//
//    private ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//            .title("人人开源")
//            .description("renren-admin文档")
//            .termsOfServiceUrl("https://www.renren.io")
//            .version("4.0.0")
//            .build();
//    }
//
//}