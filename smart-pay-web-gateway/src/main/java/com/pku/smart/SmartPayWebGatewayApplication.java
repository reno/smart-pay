package com.pku.smart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartPayWebGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartPayWebGatewayApplication.class, args);
    }

}
