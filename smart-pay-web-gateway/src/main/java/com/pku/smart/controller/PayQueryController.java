package com.pku.smart.controller;

import com.alibaba.fastjson.JSON;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicResponseParameters;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.log.MyLog;
import com.pku.smart.service.SmartPayService;
import com.pku.smart.trade.service.IPaymentManagerService;
import com.pku.smart.trade.vopackage.*;
import com.pku.smart.vopackage.TradeResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@ApiSupport(order = 11)
@Api(tags = "支付网关")
@RestController
@RequestMapping(value = "/api")
public class PayQueryController {

    private static final MyLog _log = MyLog.getLog(PayQueryController.class);

    @Autowired
    SmartPayService smartPayService;

    @Autowired
    IPaymentManagerService paymentManagerService;

    /**
     * 支付订单查询 只判断支付成功还是失败
     * @param requestVo
     * @param bindingResult
     * @return 成功：返回渠道单号 失败：失败原因 订单支付状态
     */
    @ApiOperationSupport(order = 1106)
    @ApiOperation(value = "1106 支付订单查询")
    @DynamicResponseParameters(properties = {
            @DynamicParameter(name = "retCode",value = "通讯码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCode",value = "渠道错误码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCodeDes",value = "渠道错误描述", example = "交易成功", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchId",value = "商户号", example = "10000000", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchOrderNo",value = "商户号订单号", example = "P2010203029245", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "channelOrderNo",value = "渠道订单号", example = "102348934385486857", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "invokeStatus",value = "支付状态", example = "1", required = true, dataTypeClass = String.class)
    })
    @RequestMapping(value = "/query/pay_order", method = RequestMethod.POST)
    public String queryPayOrder(@ModelAttribute VoReqQueryPay requestVo, BindingResult bindingResult) {
        _log.info("###### 开始接收商户查询支付订单请求 ######");
        TradeResultVo tradeResultVo = new TradeResultVo();
        //公共返回
        tradeResultVo.setMchId(requestVo.getMchId());
        tradeResultVo.setMchOrderNo(requestVo.getMchOrderNo());
        tradeResultVo.setChannelOrderNo(requestVo.getChannelOrderNo());

        try{
            //参数校验
            smartPayService.validateParams(requestVo,bindingResult);
            //调用服务
            VoResTradePayment<VoResQueryPay> payResultVo = paymentManagerService.payQuery(requestVo);
            //结果转换
            tradeResultVo.setRetCode(payResultVo.getRetCode());

            VoResQueryPay resQueryPay = payResultVo.getRetData();
            _log.info(">>>>>>>>>>>>返回：" + JSON.toJSONString(resQueryPay));
            //公共渠道状态
            tradeResultVo.setErrCode(resQueryPay.getErrCode());
            tradeResultVo.setErrCodeDes(resQueryPay.getErrCodeDes());

            if (PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase(payResultVo.getRetCode())){
                tradeResultVo.setMchId(resQueryPay.getMchId());
                tradeResultVo.setMchOrderNo(resQueryPay.getMchOrderNo());
                tradeResultVo.setChannelOrderNo(resQueryPay.getChannelOrderNo());
                tradeResultVo.setInvokeStatus(resQueryPay.getInvokeStatus());
            } else {
                tradeResultVo.setErrCode(resQueryPay.getErrCode());
                tradeResultVo.setErrCodeDes(resQueryPay.getErrCodeDes());
                tradeResultVo.setInvokeStatus(resQueryPay.getInvokeStatus());
            }
        } catch (Exception e) {
            e.printStackTrace();
            tradeResultVo.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            tradeResultVo.setErrCode(PayConstant.TRADE_STATUS_SYSTEM_ERROR_CODE);
            tradeResultVo.setErrCodeDes("系统异常：" + e.getMessage());
            _log.error(PayConstant.TRADE_STATUS_SYSTEM_ERROR_DESC,e);
        }

        String result = JSON.toJSONString(tradeResultVo);
        _log.info("打印结果：" + result);

        return result;
    }

    /**
     * 退款订单查询 只判断退款成功还是失败
     * @param requestVo
     * @return 成功：返回渠道退款单号 失败：失败原因 订单退款状态
     */
    @ApiOperationSupport(order = 1107)
    @ApiOperation(value = "1107 退款订单查询")
    @RequestMapping(value = "/query/refund_order", method = RequestMethod.POST)
    public String queryRefundOrder(@ModelAttribute VoReqQueryRefund requestVo, BindingResult bindingResult) {
        _log.info("###### 开始接收商户查询退款订单请求 ######");
        TradeResultVo tradeResultVo = new TradeResultVo();
        //公共返回
        tradeResultVo.setMchId(requestVo.getMchId());
        tradeResultVo.setMchOrderNo(requestVo.getMchOrderNo());
        tradeResultVo.setChannelOrderNo(requestVo.getChannelOrderNo());
        tradeResultVo.setMchRefundNo(requestVo.getMchRefundNo());
        try{
            //参数校验
            smartPayService.validateParams(requestVo,bindingResult);
            //调用服务
            VoResTradePayment<VoResQueryRefund> payResultVo = paymentManagerService.refundQuery(requestVo);
            //结果转换
            tradeResultVo.setRetCode(payResultVo.getRetCode());

            VoResQueryRefund resQueryPay = payResultVo.getRetData();
            //公共渠道状态
            tradeResultVo.setErrCode(resQueryPay.getErrCode());
            tradeResultVo.setErrCodeDes(resQueryPay.getErrCodeDes());

            if (PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase(payResultVo.getRetCode())){
                tradeResultVo.setMchId(resQueryPay.getMchId());
                tradeResultVo.setMchOrderNo(resQueryPay.getMchOrderNo());
                tradeResultVo.setChannelOrderNo(resQueryPay.getChannelOrderNo());
                tradeResultVo.setMchRefundNo(resQueryPay.getMchRefundNo());
                tradeResultVo.setChannelRefundNo(resQueryPay.getChannelRefundNo());
                tradeResultVo.setInvokeStatus(resQueryPay.getInvokeStatus());
            } else {
                tradeResultVo.setErrCode(resQueryPay.getErrCode());
                tradeResultVo.setErrCodeDes(resQueryPay.getErrCodeDes());
                tradeResultVo.setInvokeStatus(resQueryPay.getInvokeStatus());
            }
        } catch (Exception e) {
            e.printStackTrace();
            tradeResultVo.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            tradeResultVo.setErrCode(PayConstant.TRADE_STATUS_SYSTEM_ERROR_CODE);
            tradeResultVo.setErrCodeDes("系统异常：" + e.getMessage());
            _log.error(PayConstant.TRADE_STATUS_SYSTEM_ERROR_DESC,e);
        }

        String result = JSON.toJSONString(tradeResultVo);
        _log.info("打印结果：" + result);

        return result;
    }
}
