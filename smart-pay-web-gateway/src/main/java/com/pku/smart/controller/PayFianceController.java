package com.pku.smart.controller;

import com.alibaba.fastjson.JSON;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicResponseParameters;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.log.MyLog;
import com.pku.smart.service.SmartPayService;
import com.pku.smart.trade.service.IPaymentFianceService;
import com.pku.smart.trade.vopackage.VoReqDownBill;
import com.pku.smart.trade.vopackage.VoResDownBill;
import com.pku.smart.trade.vopackage.VoResTradePayment;
import com.pku.smart.vopackage.TradeResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@ApiSupport(order = 11)
@Api(tags = "支付网关")
@RestController
@RequestMapping(value = "/api")
public class PayFianceController {

    private static final MyLog _log = MyLog.getLog(PayFianceController.class);

    @Autowired
    SmartPayService smartPayService;

    @Autowired
    IPaymentFianceService paymentFianceService;

    @ApiOperationSupport(order = 1108)
    @ApiOperation(value = "1108 账单下载")
    @DynamicResponseParameters(properties = {
            @DynamicParameter(name = "retCode",value = "通讯码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCode",value = "错误码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCodeDes",value = "结果信息描述", example = "交易成功", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchId",value = "商户号", example = "10000000", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "channelName",value = "支付渠道名称", example = "WX", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "billDate",value = "账单日期", example = "2021-01-02", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "billRecords",value = "账单下载记录数", example = "11", required = true)
    })
    @RequestMapping(value = "/bill/bill_down", method = RequestMethod.POST)
    public String codePay(@ModelAttribute VoReqDownBill requestVo, BindingResult bindingResult) {
        _log.info("###### 开始接收商户统一账单下载请求 ######");
        TradeResultVo tradeResultVo = new TradeResultVo();
        //公共返回
        tradeResultVo.setMchId(requestVo.getMchId());

        try{
            //参数校验
            smartPayService.validateParams(requestVo, bindingResult);
            //调用服务
            VoResTradePayment<VoResDownBill> payResultVo = paymentFianceService.downBill(requestVo);
            //结果转换
            tradeResultVo.setRetCode(payResultVo.getRetCode());
            //默认值
            VoResDownBill resDownBill = payResultVo.getRetData();
            _log.info(">>>>>>>>>>>>返回：" + JSON.toJSONString(resDownBill));
            //公共渠道状态
            tradeResultVo.setErrCode(resDownBill.getErrCode());
            tradeResultVo.setErrCodeDes(resDownBill.getErrCodeDes());

            tradeResultVo.setErrCode(resDownBill.getErrCode());
            tradeResultVo.setErrCodeDes(resDownBill.getErrCodeDes());
            tradeResultVo.setTotalCount(resDownBill.getBillRecords().toString());
            tradeResultVo.setBillDate(requestVo.getBillDate());
        } catch (Exception e) {
            e.printStackTrace();
            tradeResultVo.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            tradeResultVo.setErrCode(PayConstant.TRADE_STATUS_SYSTEM_ERROR_CODE);
            tradeResultVo.setErrCodeDes("系统异常：" + e.getMessage());
            tradeResultVo.setBillDate(requestVo.getBillDate());
            _log.error(PayConstant.TRADE_STATUS_SYSTEM_ERROR_DESC, e);
        }

        String result = JSON.toJSONString(tradeResultVo);
        _log.info("打印结果：" + result);

        return result;
    }
}
