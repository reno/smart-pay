package com.pku.smart.controller;

import com.alibaba.fastjson.JSON;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicResponseParameters;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.log.MyLog;
import com.pku.smart.service.SmartPayService;
import com.pku.smart.trade.service.IPaymentManagerService;
import com.pku.smart.trade.vopackage.VoReqPayScan;
import com.pku.smart.trade.vopackage.VoResPayScan;
import com.pku.smart.trade.vopackage.VoResTradePayment;
import com.pku.smart.vopackage.TradeResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 扫码支付
 * 用户通过手机扫描商户二维码
 */
@ApiSupport(order = 11)
@Api(tags = "支付网关")
@RestController
@RequestMapping(value = "/api")
public class PayScanController {
    private static final MyLog _log = MyLog.getLog(PayScanController.class);

    @Autowired
    SmartPayService payService;

    @Autowired
    IPaymentManagerService paymentManagerService;

    /**
     * 扫码支付
     * @param requestVo mchId channelId mchOrderNo totalAmount subject body notifyUrl clientIp device extra sign
     * @param bindingResult 校验必填字段 mchId channelId mchOrderNo totalAmount subject notifyUrl clientIp sign
     * @return 成功：返回二维码 失败：返回失败原因
     */
    @ApiOperationSupport(order = 1102)
    @ApiOperation(value = "1102 扫码支付")
    @DynamicResponseParameters(properties = {
            @DynamicParameter(name = "retCode",value = "通讯码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCode",value = "错误码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCodeDes",value = "结果信息描述", example = "交易成功", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchId",value = "商户号", example = "10000000", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchOrderNo",value = "商户号订单号", example = "P2010203029245", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "codeUrl",value = "支付二维码", example = "wxpay://weefg.cv", required = true, dataTypeClass = String.class)
    })
    @RequestMapping(value = "/pay/create_order", method = RequestMethod.POST)
    public String scanPay(@ModelAttribute VoReqPayScan requestVo, BindingResult bindingResult) {
        _log.info("###### 开始接收商户统一扫码支付请求 ######");
        TradeResultVo tradeResultVo = new TradeResultVo();
        //公共返回
        tradeResultVo.setMchId(requestVo.getMchId());
        tradeResultVo.setMchOrderNo(requestVo.getMchOrderNo());
        tradeResultVo.setChannelOrderNo("");

        try {
            //参数校验
            payService.validateParams(requestVo,bindingResult);
            //调用服务
            VoResTradePayment<VoResPayScan> payResultVo = paymentManagerService.scanPay(requestVo);
            //结果转换
            tradeResultVo.setRetCode(payResultVo.getRetCode());

            VoResPayScan resPayScan = payResultVo.getRetData();
            _log.info(">>>>>>>>>>>>返回：" + JSON.toJSONString(resPayScan));
            //公共渠道状态
            tradeResultVo.setErrCode(resPayScan.getErrCode());
            tradeResultVo.setErrCodeDes(resPayScan.getErrCodeDes());

            if (PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase(payResultVo.getRetCode())){
                tradeResultVo.setMchId(resPayScan.getMchId());
                tradeResultVo.setMchOrderNo(resPayScan.getMchOrderNo());
                tradeResultVo.setCodeUrl(resPayScan.getCodeUrl());
            } else {
                tradeResultVo.setErrCode(resPayScan.getErrCode());
                tradeResultVo.setErrCodeDes(resPayScan.getErrCodeDes());
            }
        } catch (Exception e) {
            e.printStackTrace();
            tradeResultVo.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            tradeResultVo.setErrCode(PayConstant.TRADE_STATUS_SYSTEM_ERROR_CODE);
            tradeResultVo.setErrCodeDes("系统异常：" + e.getMessage());
            _log.error(PayConstant.TRADE_STATUS_SYSTEM_ERROR_DESC,e);
        }

        String result = JSON.toJSONString(tradeResultVo);
        _log.info("打印结果：" + result);

        return result;
    }
}
